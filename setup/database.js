var mysql = require('mysql2');
require('dotenv').config();
const bcrypt = require('bcrypt');

var con = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USER_DB,
  password: process.env.PASSWORD,
  database: process.env.DATABASE
});

con.connect(function(err) {
  if (err) throw err;
  var sqlUser = "CREATE TABLE user (id INT AUTO_INCREMENT PRIMARY KEY, team_id INT, name VARCHAR(255) UNIQUE, password VARCHAR(255), email VARCHAR(255), admin BOOLEAN DEFAULT 0)";
  var sqlLeague = "CREATE TABLE league (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), year VARCHAR(255))";
  var sqlCompetition = "CREATE TABLE competition (id INT AUTO_INCREMENT PRIMARY KEY, competition_group_id INT, name VARCHAR(255))";
  var sqlCompetitionGroup = "CREATE TABLE competition_group (id INT AUTO_INCREMENT PRIMARY KEY, league_id INT, name VARCHAR(255), start_date DATETIME, end_date DATETIME, active BOOLEAN)";
  var sqlBasketballCourt = "CREATE TABLE basketball_court (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), direction VARCHAR(255), status VARCHAR(255), lights BOOLEAN DEFAULT 0, light_timeout VARCHAR(255))";
  var sqlTeam = "CREATE TABLE team (id INT AUTO_INCREMENT PRIMARY KEY, league_id INT, name VARCHAR(255), photo MEDIUMTEXT)";
  var sqlGame = "CREATE TABLE game (id INT AUTO_INCREMENT PRIMARY KEY, competition_id INT, court_id INT, local_id INT, visitant_id INT, date DATETIME, local_score INT DEFAULT 0, visitant_score INT DEFAULT 0, local_fair_play INT DEFAULT 0, visitant_fair_play INT DEFAULT 0, local_mixed BOOLEAN, visitant_mixed BOOLEAN, finished BOOLEAN, confirmed BOOLEAN)";
  var sqlCompetitionTeam = "CREATE TABLE competition_team (id INT AUTO_INCREMENT PRIMARY KEY, competition_id INT, team_id INT, victories INT DEFAULT 0, defeats INT DEFAULT 0, points_scored INT DEFAULT 0, points_received INT DEFAULT 0, competition_points INT DEFAULT 0, fair_play INT DEFAULT 0, mixed_games INT DEFAULT 0)";

  con.query(sqlUser, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlLeague, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlCompetition, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlCompetitionGroup, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlBasketballCourt, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlTeam, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlGame, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  con.query(sqlCompetitionTeam, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });

  // FIRST USER
  const hash = bcrypt.hashSync("098f6bcd4621d373cade4e832627b4f6", Number(process.env.SALT));
  var sqlFirstUser = "INSERT INTO user ( team_id, name, password, email, admin ) VALUES (null, 'SuperAdmin', '" + hash + "','test',1)";
  const hash2 = bcrypt.hashSync("test", Number(process.env.SALT));
  var sqlSecondUser = "INSERT INTO user ( team_id, name, password, email, admin ) VALUES (null, 'SuperAdmin2', '" + hash2 + "','test2',1)";

  con.query(sqlFirstUser, function (err, result) {
    if (err) throw err;
    console.log("First user added");
  });

  con.end();
});
