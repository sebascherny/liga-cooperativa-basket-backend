# liga-cooperativa-basket-backend
Aplicación cooperativa pa organizarte una liga to wapa de lo que quieras en las canchas de tu barrio

Setup (Crear bbdd y conexión):<br />
1 - Crear una conexión mysql<br />
2 - Actualizar los datos de la conexión en  `node setup/database.js` y en `src\dataSources\mysql.datasource.ts`<br />
3 - Ejecutar `node setup/database.js` en la carpeta del repositorio<br />
<br />

Compilar el código:<br />
1 - Ejecutar `tsc` en la carpeta del repositorio para compilar los cambios<br />

Lanzar la app:<br />
1 - Instalar NodeJS v8.9.4 o posterior<br />
3 - Clonar el repo liga-cooperativa-basket-backend<br />
4 - Ejecutar `npm install` en la carpeta del repositorio<br />
5 - Ejecutar `node dist/index.js` en la carpeta del repositorio y esperar al mensaje "API escuchando en el puerto 8080"<br />
<br />

Tests:<br />
En el repo está el archivo: `Liga Cooperativa Basket.postman_collection.json` con una colección de postman para probar la app.<br />

Ejemplo:
CURL para crear una cancha:
`curl --location 'http://127.0.0.1:8080/basketball-courts' \
--header 'Content-Type: application/json' \
--data '{
    "name":"ef",
    "direction":"fsf",
    "status":"fss",
    "lights":"true",
    "lightTimeout":"21:00"
}'`

CURL para describir una cancha:
`curl --location 'http://127.0.0.1:8080/basketball-courts/1'`
