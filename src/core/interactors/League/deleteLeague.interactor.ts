import {League} from '../../entities/League';
import DataRepository from '../../repositories/league.repository';

import LeagueDatasource from '../../../dataSources/mysql/league.datasource';

const deleteLeague = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteLeague(id);
  return "Affected rows = "+affectedRows;
}

const leagueDatasource = new LeagueDatasource();
export default deleteLeague( leagueDatasource );
