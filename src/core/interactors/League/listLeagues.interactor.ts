import {League} from '../../entities/League';
import DataRepository from '../../repositories/league.repository';

import LeagueDatasource from '../../../dataSources/mysql/league.datasource';

const listLeagues = (dataRepository: DataRepository) => async (): Promise<League[]> => {
  const LeagueItems: League[] = await dataRepository.listLeagues();
  return LeagueItems;
}

const leagueDatasource = new LeagueDatasource();
export default listLeagues( leagueDatasource );
