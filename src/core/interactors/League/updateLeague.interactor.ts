import { League, UpdateLeagueInput } from '../../entities/League';
import DataRepository from '../../repositories/league.repository';

import LeagueDatasource from '../../../dataSources/mysql/league.datasource';

const updateLeague = (dataRepository: DataRepository) => async ( updateLeagueInput :UpdateLeagueInput ): Promise<League> => {

  let leagueItem = await dataRepository.getLeague(updateLeagueInput.id);
  if (updateLeagueInput.name != null){
    leagueItem.name = updateLeagueInput.name;
  }
  if (updateLeagueInput.year != null){
    leagueItem.year = updateLeagueInput.year;
  }

  let affectedRows = await dataRepository.updateLeague(leagueItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return leagueItem;
}

const leagueDatasource = new LeagueDatasource();
export default updateLeague( leagueDatasource );
