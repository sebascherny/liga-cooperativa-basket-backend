import {League} from '../../entities/League';
import DataRepository from '../../repositories/league.repository';

import LeagueDatasource from '../../../dataSources/mysql/league.datasource';

const getLeague = (dataRepository: DataRepository) => async ( id: number ): Promise<League> => {
  const LeagueItem: League = await dataRepository.getLeague(id);
  return LeagueItem;
}

const leagueDatasource = new LeagueDatasource();
export default getLeague( leagueDatasource );
