import {League, AddLeagueInput} from '../../entities/League';
import DataRepository from '../../repositories/league.repository';

import LeagueDatasource from '../../../dataSources/mysql/league.datasource';

const addLeague = (dataRepository: DataRepository) => async ( addLeagueInput :AddLeagueInput ): Promise<League> => {

  let leagueItem = <League>{};
  leagueItem.name = addLeagueInput.name;
  leagueItem.year = addLeagueInput.year;

  let leagueId = await dataRepository.addLeague(leagueItem);
  leagueItem.id = leagueId;
  return leagueItem;
}

const leagueDatasource = new LeagueDatasource();
export default addLeague( leagueDatasource );
