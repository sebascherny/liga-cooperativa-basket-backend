import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';

import TeamDatasource from '../../../dataSources/mysql/team.datasource';

const deleteTeam = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteTeam(id);
  return "Affected rows = "+affectedRows;
}

const teamDatasource = new TeamDatasource();
export default deleteTeam( teamDatasource );
