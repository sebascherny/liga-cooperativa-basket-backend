import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';
import TeamDatasource from '../../../dataSources/mysql/team.datasource';

import { Game, GameFilters } from '../../entities/Game';
import listGames from '../Game/listGames.interactor';

import { CompetitionTeam } from '../../entities/CompetitionTeam';
import getTeams from '../CompetitionTeam/getTeams.interactor';

const listTeamsPendingGames = (dataRepository: DataRepository) => async ( competitionId: string, teamId: string ): Promise<Team[]> => {
  const TeamItems: Team[] = await getTeams(Number(competitionId));

  //Remove teams
  const index = TeamItems.findIndex((i) => {
    return i.id === Number(teamId);
  })

  if (index != -1 ){
    TeamItems.splice(index, 1);
  }

  // Local Games
  let filtersLocal = <GameFilters>{};
  filtersLocal.localId = teamId;
  filtersLocal.competitionId = competitionId;

  let localGames :Game[] = await listGames(filtersLocal);

  if (localGames.length != undefined )  {
    for (let game of localGames) {
      const index = TeamItems.findIndex((i) => {
        return i.id === Number(game.visitantId);
      })

      if (index != -1 ){
        TeamItems.splice(index, 1);
      }
    }
  }

  //Visitant Games
  let filtersVisitant = <GameFilters>{};
  filtersVisitant.visitantId = teamId;
  filtersVisitant.competitionId = competitionId;
  let visitantGames :Game[] = await listGames(filtersVisitant);
  if (visitantGames.length != undefined )  {
    for (let game of visitantGames) {
      const index = TeamItems.findIndex((i) => {
        return i.id === Number(game.localId);
      })

      if (index != -1 ){
        TeamItems.splice(index, 1);
      }
    }
  }

  return TeamItems;
}

const teamDatasource = new TeamDatasource();
export default listTeamsPendingGames( teamDatasource );
