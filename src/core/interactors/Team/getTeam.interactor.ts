import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';

import TeamDatasource from '../../../dataSources/mysql/team.datasource';

const getTeam = (dataRepository: DataRepository) => async ( id: number ): Promise<Team> => {
  const TeamItem: Team = await dataRepository.getTeam(id);
  return TeamItem;
}

const teamDatasource = new TeamDatasource();
export default getTeam( teamDatasource );
