import { Team, AddTeamInput, TeamDefaults } from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';

import TeamDatasource from '../../../dataSources/mysql/team.datasource';
import getLeague from '../League/getLeague.interactor';

const addTeam = (dataRepository: DataRepository) => async ( addTeamInput :AddTeamInput ): Promise<Team> => {

  let league = await getLeague(addTeamInput.leagueId);
  let teamItem = <Team>{};

  teamItem.photo = addTeamInput.photo;
  if (addTeamInput.photo == null || addTeamInput.photo == "") {
    teamItem.photo = TeamDefaults.photo;
  }

  teamItem.leagueId = addTeamInput.leagueId;
  teamItem.name = addTeamInput.name;

  let teamId = await dataRepository.addTeam(teamItem);
  teamItem.id = teamId;

  return teamItem;
}

const teamDatasource = new TeamDatasource();
export default addTeam( teamDatasource );
