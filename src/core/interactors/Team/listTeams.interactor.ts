import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';

import TeamDatasource from '../../../dataSources/mysql/team.datasource';

const listTeams = (dataRepository: DataRepository) => async (): Promise<Team[]> => {

  const TeamItems: Team[] = await dataRepository.listTeams();

  return TeamItems;
}

const teamDatasource = new TeamDatasource();
export default listTeams( teamDatasource );
