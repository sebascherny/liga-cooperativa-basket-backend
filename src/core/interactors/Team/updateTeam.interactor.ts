import { Team, UpdateTeamInput } from '../../entities/Team';
import DataRepository from '../../repositories/team.repository';

import TeamDatasource from '../../../dataSources/mysql/team.datasource';
import getLeague from '../League/getLeague.interactor';

const updateTeam = (dataRepository: DataRepository) => async ( updateTeamInput :UpdateTeamInput ): Promise<Team> => {

  let teamItem = await dataRepository.getTeam(updateTeamInput.id);
  if (updateTeamInput.leagueId != null){
    let league = await getLeague(updateTeamInput.leagueId);
    teamItem.leagueId = updateTeamInput.leagueId;
  }
  if (updateTeamInput.name != null){
    teamItem.name = updateTeamInput.name;
  }
  if (updateTeamInput.photo != null){
    teamItem.photo = updateTeamInput.photo;
  }

  let affectedRows = await dataRepository.updateTeam(teamItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return teamItem;
}

const teamDatasource = new TeamDatasource();
export default updateTeam( teamDatasource );
