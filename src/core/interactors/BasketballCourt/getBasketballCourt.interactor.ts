import {BasketballCourt} from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';

import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

const getBasketballCourt = (dataRepository: DataRepository) => async ( id: number ): Promise<BasketballCourt> => {

  const BasketballCourtItem: BasketballCourt = await dataRepository.getBasketballCourt(id);

  return BasketballCourtItem;
}

const basketballCourtDatasource = new BasketballCourtDatasource();
export default getBasketballCourt( basketballCourtDatasource );
