import {BasketballCourt} from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';
import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

import { Game, GameFilters, GameDefaults } from '../../entities/Game';
import listGames from '../Game/listGames.interactor';

const listAvailableBasketballCourts = (dataRepository: DataRepository) => async ( date:string ): Promise<BasketballCourt[]> => {

  let BasketballCourtItems: BasketballCourt[] = await dataRepository.listBasketballCourts();

  let filters = <GameFilters>{};
  let gameDate = isoStrToDate(date);

  let gameDateMlSeconds = gameDate.getTime();
  let defaultGameMlSeconds = GameDefaults.gameDuration * 60 * 1000;
  let startDate = new Date(gameDateMlSeconds - defaultGameMlSeconds);
  let endDate = new Date(gameDateMlSeconds + defaultGameMlSeconds);

  filters.startDate =  startDate.toISOString();
  filters.endDate =  endDate.toISOString();
  filters.finished = 'false';

  const GameItems: Game[] = await listGames(filters);

  if (GameItems.length != undefined )  {
    for (let game of GameItems) {
      const index = BasketballCourtItems.findIndex((i) => {
        return i.id === game.courtId;
      })

      if (index != -1 ){
        BasketballCourtItems.splice(index, 1);
      }
    }
  }
  return BasketballCourtItems;
}

function isoStrToDate(dtStr) {
  if (!dtStr) return null
  var b = dtStr.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}


const basketballCourtDatasource = new BasketballCourtDatasource();
export default listAvailableBasketballCourts( basketballCourtDatasource );
