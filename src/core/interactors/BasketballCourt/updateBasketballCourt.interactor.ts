import { BasketballCourt, UpdateBasketballCourtInput } from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';

import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

const updateBasketballCourt = (dataRepository: DataRepository) => async ( updateBasketballCourtInput :UpdateBasketballCourtInput ): Promise<BasketballCourt> => {

  let basketballCourtItem = await dataRepository.getBasketballCourt(updateBasketballCourtInput.id);

  if (updateBasketballCourtInput.name != null){
    basketballCourtItem.name = updateBasketballCourtInput.name;
  }
  if (updateBasketballCourtInput.direction != null){
    basketballCourtItem.direction = updateBasketballCourtInput.direction;
  }
  if (updateBasketballCourtInput.status != null){
    basketballCourtItem.status = updateBasketballCourtInput.status;
  }
  if (updateBasketballCourtInput.lights != null){
    basketballCourtItem.lights = updateBasketballCourtInput.lights;
  }
  if (updateBasketballCourtInput.lightTimeout != null){
    basketballCourtItem.lightTimeout = updateBasketballCourtInput.lightTimeout;
  }

  let affectedRows = await dataRepository.updateBasketballCourt(basketballCourtItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return basketballCourtItem;
}

const basketballCourtDatasource = new BasketballCourtDatasource();
export default updateBasketballCourt( basketballCourtDatasource );
