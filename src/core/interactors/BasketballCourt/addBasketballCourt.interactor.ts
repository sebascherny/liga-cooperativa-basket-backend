import { BasketballCourt, AddBasketballCourtInput } from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';

import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

const addBasketballCourt = (dataRepository: DataRepository) => async ( addBasketballCourtInput :AddBasketballCourtInput ): Promise<BasketballCourt> => {

  let basketballCourtItem = <BasketballCourt>{};
  basketballCourtItem.name = addBasketballCourtInput.name;
  basketballCourtItem.direction = addBasketballCourtInput.direction;
  basketballCourtItem.status = addBasketballCourtInput.status;
  basketballCourtItem.lights = addBasketballCourtInput.lights;
  basketballCourtItem.lightTimeout = addBasketballCourtInput.lightTimeout;

  let basketballCourtId = await dataRepository.addBasketballCourt(basketballCourtItem);
  basketballCourtItem.id = basketballCourtId;

  return basketballCourtItem;
}

const basketballCourtDatasource = new BasketballCourtDatasource();
export default addBasketballCourt( basketballCourtDatasource );
