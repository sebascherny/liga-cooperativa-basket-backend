import {BasketballCourt} from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';

import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

const deleteBasketballCourt = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteBasketballCourt(id);
  return "Affected rows = "+affectedRows;
}

const basketballCourtDatasource = new BasketballCourtDatasource();
export default deleteBasketballCourt( basketballCourtDatasource );
