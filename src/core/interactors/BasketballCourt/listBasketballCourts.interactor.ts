import {BasketballCourt} from '../../entities/BasketballCourt';
import DataRepository from '../../repositories/basketballCourt.repository';
import BasketballCourtDatasource from '../../../dataSources/mysql/basketballCourt.datasource';

const listBasketballCourts = (dataRepository: DataRepository) => async (): Promise<BasketballCourt[]> => {
  const BasketballCourtItems: BasketballCourt[] = await dataRepository.listBasketballCourts();
  return BasketballCourtItems;
}

const basketballCourtDatasource = new BasketballCourtDatasource();
export default listBasketballCourts( basketballCourtDatasource );
