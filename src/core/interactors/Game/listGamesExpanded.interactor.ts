import { Game, GameExpanded, GameFilters } from '../../entities/Game';
import {Competition} from '../../entities/Competition';
import {BasketballCourt} from '../../entities/BasketballCourt';
import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/game.repository';

import getTeam from '../Team/getTeam.interactor';
import getCompetition from '../Competition/getCompetition.interactor';
import getBasketballCourt from '../BasketballCourt/getBasketballCourt.interactor';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

const listGamesExpanded = (dataRepository: DataRepository) => async (filters: GameFilters): Promise<GameExpanded[]> => {

  if (filters.startDate != null) {
    let startDate = isoStrToDate(filters.startDate);
    filters.startDate =  dateToLocalTimezoneStr(startDate);
  }
  if (filters.endDate != null) {
    let endDate = isoStrToDate(filters.endDate);
    filters.endDate =  dateToLocalTimezoneStr(endDate);
  }


  const GameItems: Game[] = await dataRepository.listGames(filters);
  let games: GameExpanded[] = [];

  for (let game of GameItems) {
    let gameExpandedItem = <GameExpanded>{};

    gameExpandedItem.id = game.id;
    gameExpandedItem.competition = await getCompetition(Number(game.competitionId));
    gameExpandedItem.court = await getBasketballCourt(Number(game.courtId));
    gameExpandedItem.local = await getTeam(Number(game.localId));
    gameExpandedItem.visitant = await getTeam(Number(game.visitantId));
    gameExpandedItem.date = game.date;
    gameExpandedItem.localScore = game.localScore;
    gameExpandedItem.visitantScore = game.visitantScore;
    gameExpandedItem.localMixed = game.localMixed;
    gameExpandedItem.visitantMixed = game.visitantMixed;
    gameExpandedItem.finished = game.finished;
    gameExpandedItem.confirmed = game.confirmed;

    games.push(gameExpandedItem);
  }

  return games;
}

function dateToLocalTimezoneStr(date) {
  if (!date) return null
  return [date.getFullYear(),
             date.getMonth()+1,
             date.getDate()].join('-')+' '+
             [date.toLocaleTimeString()].join(':');
}

function isoStrToDate(dtStr) {
  if (!dtStr) return null
  var b = dtStr.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

const gameDatasource = new GameDatasource();
export default listGamesExpanded( gameDatasource );
