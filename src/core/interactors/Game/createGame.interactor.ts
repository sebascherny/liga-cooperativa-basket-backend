import { Game, CreateGameInput, GameFilters } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import getTeam from '../Team/getTeam.interactor';
import getCompetition from '../Competition/getCompetition.interactor';
import getCompetitionTeam from '../CompetitionTeam/getCompetitionTeam.interactor';
import getBasketballCourt from '../BasketballCourt/getBasketballCourt.interactor';
import listGames from './listGames.interactor';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

const createGame = (dataRepository: DataRepository) => async ( createGameInput :CreateGameInput ): Promise<Game> => {

  let gameItem = <Game>{};

  let competition = await getCompetition(createGameInput.competitionId);
  gameItem.competitionId = createGameInput.competitionId;

  let court = await getBasketballCourt(createGameInput.courtId);
  gameItem.courtId = createGameInput.courtId;

  let local = await getTeam(createGameInput.localId);
  let localCompetitionTeam = await getCompetitionTeam(createGameInput.competitionId, createGameInput.localId);
  gameItem.localId = createGameInput.localId;

  let visitant = await getTeam(createGameInput.visitantId);
  let visitantCompetitionTeam = await getCompetitionTeam(createGameInput.competitionId, createGameInput.visitantId);
  gameItem.visitantId = createGameInput.visitantId;

  gameItem.date = createGameInput.date;
  if (createGameInput.date == null) {
    gameItem.date = new Date();
  }

  let gameFilters = <GameFilters>{};
  gameFilters.competitionId = gameItem.competitionId.toString();
  gameFilters.localId = gameItem.localId.toString();
  gameFilters.visitantId = gameItem.visitantId.toString();
  gameFilters.finished = 'false';

  let games = await listGames(gameFilters);
  if(games.length > 0){
    throw Error("Este partido ya está creado y no se ha jugado")
  }

  gameItem.localScore = 0;
  gameItem.visitantScore = 0;
  gameItem.localFairPlay = 0;
  gameItem.visitantFairPlay = 0;
  gameItem.localMixed = false;
  gameItem.visitantMixed = false;
  gameItem.finished = false;
  gameItem.confirmed = false;

  let gameId = await dataRepository.addGame(gameItem);
  gameItem.id = gameId;

  return gameItem;
}

const gameDatasource = new GameDatasource();
export default createGame( gameDatasource );
