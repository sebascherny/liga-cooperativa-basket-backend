import { Game, GameDefaults, AddGameInput } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

import getCompetition from '../Competition/getCompetition.interactor';
import getBasketballCourt from '../BasketballCourt/getBasketballCourt.interactor';
import getTeam from '../Team/getTeam.interactor';
import getCompetitionTeam from '../CompetitionTeam/getCompetitionTeam.interactor';

const addGame = (dataRepository: DataRepository) => async (addGameInput :AddGameInput): Promise<Game> => {

  let competition = await getCompetition(addGameInput.competitionId);
  let court = await getBasketballCourt(addGameInput.courtId);
  let local = await getTeam(addGameInput.localId);
  let localCompetitionTeam = await getCompetitionTeam(addGameInput.competitionId, addGameInput.localId);
  let visitant = await getTeam(addGameInput.visitantId);
  let visitantCompetitionTeam = await getCompetitionTeam(addGameInput.competitionId, addGameInput.visitantId);

  let gameItem = <Game>{};
  gameItem.competitionId = addGameInput.competitionId;
  gameItem.courtId = addGameInput.courtId;
  gameItem.localId = addGameInput.localId;
  gameItem.visitantId = addGameInput.visitantId;

  gameItem.date = addGameInput.date;
  if (addGameInput.date == null) {
    gameItem.date = new Date();
  }
  gameItem.localScore = addGameInput.localScore;
  if (addGameInput.localScore == null) {
    gameItem.localScore = GameDefaults.localScore;
  }
  gameItem.visitantScore = addGameInput.visitantScore;
  if (addGameInput.visitantScore == null) {
    gameItem.visitantScore = GameDefaults.visitantScore;
  }
  gameItem.localFairPlay = addGameInput.localFairPlay;
  if (addGameInput.localFairPlay == null) {
    gameItem.localFairPlay = GameDefaults.localFairPlay;
  }
  gameItem.visitantFairPlay = addGameInput.visitantFairPlay;
  if (addGameInput.visitantFairPlay == null) {
    gameItem.visitantFairPlay = GameDefaults.visitantFairPlay;
  }
  gameItem.localMixed = addGameInput.localMixed;
  if (addGameInput.localMixed == null) {
    gameItem.localMixed = GameDefaults.localMixed;
  }
  gameItem.visitantMixed = addGameInput.visitantMixed;
  if (addGameInput.visitantMixed == null) {
    gameItem.visitantMixed = GameDefaults.visitantMixed;
  }
  gameItem.finished = addGameInput.finished;
  if (addGameInput.finished == null) {
    gameItem.finished = GameDefaults.finished;
  }
  gameItem.confirmed = addGameInput.confirmed;
  if (addGameInput.confirmed == null) {
    gameItem.confirmed = GameDefaults.confirmed;
  }

  let gameId = await dataRepository.addGame(gameItem);
  gameItem.id = gameId;

  return gameItem;
}

const gameDatasource = new GameDatasource();
export default addGame( gameDatasource );
