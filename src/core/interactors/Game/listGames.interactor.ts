import { Game, GameFilters } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

const listGames = (dataRepository: DataRepository) => async (filters: GameFilters): Promise<Game[]> => {

  let startDate = isoStrToDate(filters.startDate);
  let endDate = isoStrToDate(filters.endDate);

  filters.startDate =  dateToLocalTimezoneStr(startDate);
  filters.endDate =  dateToLocalTimezoneStr(endDate);

  const GameItems: Game[] = await dataRepository.listGames(filters);
  return GameItems;
}

const gameDatasource = new GameDatasource();
export default listGames( gameDatasource );

function dateToLocalTimezoneStr(date) {
  if (!date) return null
  var offset = new Date().getTimezoneOffset();

  return [date.getFullYear(),
             date.getMonth()+1,
             date.getDate()].join('-')+' '+
         [date.getHours()-(offset/60),
           date.getMinutes()]
           .join(':');
}

function isoStrToDate(dtStr) {
  if (!dtStr) return null
  var b = dtStr.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5]));
}
