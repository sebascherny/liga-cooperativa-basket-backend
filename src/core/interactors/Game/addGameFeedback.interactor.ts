import { Game, AddGameFeedbackInput } from '../../entities/Game';
import {UpdateCompetitionTeamInput} from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';
import getCompetitionTeam from '../CompetitionTeam/getCompetitionTeam.interactor';
import updateCompetitionTeam from '../CompetitionTeam/updateCompetitionTeam.interactor';
import getGame from './getGame.interactor'

const addGameFeedback = (dataRepository: DataRepository) => async ( addGameFeedbackInput :AddGameFeedbackInput ): Promise<Game> => {

  let game = await getGame(addGameFeedbackInput.id);

  if( game.confirmed ){
    throw Error("El fairplay de este partido ya se ha subido")
  }

  let gameItem = <Game>{};
  gameItem.id = addGameFeedbackInput.id;
  gameItem.localFairPlay = addGameFeedbackInput.localFairPlay;
  gameItem.localMixed = addGameFeedbackInput.localMixed;
  gameItem.confirmed = true;

  let affectedRows = await dataRepository.addGameFeedback(gameItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  let local = await getCompetitionTeam(Number(game.competitionId),Number(game.localId));

  local.fairPlay = Number(local.fairPlay) + Number(addGameFeedbackInput.localFairPlay);

  if (addGameFeedbackInput.localMixed) {
    local.mixedGames = Number(local.mixedGames) + 1;
  }

  let localCompetitionTeamItem = <UpdateCompetitionTeamInput>{};
  localCompetitionTeamItem.competitionId = Number(local.competitionId);
  localCompetitionTeamItem.teamId = Number(local.teamId);
  localCompetitionTeamItem.fairPlay = local.fairPlay;
  localCompetitionTeamItem.mixedGames = local.mixedGames;

  await updateCompetitionTeam(localCompetitionTeamItem);

  return gameItem;
}

const gameDatasource = new GameDatasource();
export default addGameFeedback( gameDatasource );
