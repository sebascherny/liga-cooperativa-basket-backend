import { Game } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

const getGame = (dataRepository: DataRepository) => async ( id: number ): Promise<Game> => {

  const GameItem: Game = await dataRepository.getGame(id);

  return GameItem;
}

const gameDatasource = new GameDatasource();
export default getGame( gameDatasource );
