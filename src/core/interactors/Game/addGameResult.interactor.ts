import { Game, GameDefaults, AddGameResultInput } from '../../entities/Game';
import {UpdateCompetitionTeamInput} from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';
import getCompetitionTeam from '../CompetitionTeam/getCompetitionTeam.interactor';
import updateCompetitionTeam from '../CompetitionTeam/updateCompetitionTeam.interactor';
import getGame from './getGame.interactor'

const addGameResult = (dataRepository: DataRepository) => async ( addGameResultInput :AddGameResultInput ): Promise<Game> => {

  let game = await getGame(addGameResultInput.id);

  if( game.finished ){
    throw Error("El resultado de este partido ya se ha subido")
  }

  let gameItem = <Game>{};
  gameItem.id = addGameResultInput.id;
  gameItem.localScore = addGameResultInput.localScore;
  gameItem.visitantScore = addGameResultInput.visitantScore;
  gameItem.visitantFairPlay = addGameResultInput.visitantFairPlay;
  gameItem.visitantMixed = addGameResultInput.visitantMixed;
  gameItem.finished = true;

  let affectedRows = await dataRepository.addGameResult(gameItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  let local = await getCompetitionTeam(Number(game.competitionId),Number(game.localId));
  let visitant = await getCompetitionTeam(Number(game.competitionId),Number(game.visitantId));

  visitant.fairPlay = Number(visitant.fairPlay) + Number(addGameResultInput.visitantFairPlay);

  if (addGameResultInput.visitantMixed) {
    visitant.mixedGames = Number(visitant.mixedGames) + 1;
  }

  if (Number(addGameResultInput.localScore) > Number(addGameResultInput.visitantScore)) {
    local.victories++;
    visitant.defeats++;
    local.competitionPoints = Number(local.competitionPoints) + GameDefaults.victoryPoints;
    visitant.competitionPoints = Number(visitant.competitionPoints) + GameDefaults.defeatPoints;
  } else if (Number(addGameResultInput.localScore) < Number(addGameResultInput.visitantScore)){
    local.defeats++;
    visitant.victories++;
    local.competitionPoints = Number(local.competitionPoints) + GameDefaults.defeatPoints;
    visitant.competitionPoints = Number(visitant.competitionPoints) + GameDefaults.victoryPoints;
  } else {
    local.competitionPoints = Number(local.competitionPoints) + GameDefaults.defeatPoints;
    visitant.competitionPoints = Number(visitant.competitionPoints) + GameDefaults.defeatPoints;
  }

  local.pointsScored = Number(local.pointsScored) + Number(addGameResultInput.localScore);
  local.pointsReceived = Number(local.pointsReceived) + Number(addGameResultInput.visitantScore);
  visitant.pointsScored = Number(visitant.pointsScored) + Number(addGameResultInput.visitantScore);
  visitant.pointsReceived = Number(visitant.pointsReceived) + Number(addGameResultInput.localScore);

  let localCompetitionTeamItem = <UpdateCompetitionTeamInput>{};
  localCompetitionTeamItem.competitionId = Number(local.competitionId);
  localCompetitionTeamItem.teamId = Number(local.teamId);
  localCompetitionTeamItem.victories = local.victories;
  localCompetitionTeamItem.defeats = local.defeats;
  localCompetitionTeamItem.pointsScored = local.pointsScored;
  localCompetitionTeamItem.pointsReceived = local.pointsReceived;
  localCompetitionTeamItem.competitionPoints = local.competitionPoints;
  localCompetitionTeamItem.fairPlay = local.fairPlay;
  localCompetitionTeamItem.mixedGames = local.mixedGames;
  await updateCompetitionTeam(localCompetitionTeamItem);

  let visitantCompetitionTeamItem = <UpdateCompetitionTeamInput>{};
  visitantCompetitionTeamItem.competitionId = Number(visitant.competitionId);
  visitantCompetitionTeamItem.teamId = Number(visitant.teamId);
  visitantCompetitionTeamItem.victories = visitant.victories;
  visitantCompetitionTeamItem.defeats = visitant.defeats;
  visitantCompetitionTeamItem.pointsScored = visitant.pointsScored;
  visitantCompetitionTeamItem.pointsReceived = visitant.pointsReceived;
  visitantCompetitionTeamItem.competitionPoints = visitant.competitionPoints;
  visitantCompetitionTeamItem.fairPlay = visitant.fairPlay;
  visitantCompetitionTeamItem.mixedGames = visitant.mixedGames;
  await updateCompetitionTeam(visitantCompetitionTeamItem);

  return gameItem;
}

const gameDatasource = new GameDatasource();
export default addGameResult( gameDatasource );
