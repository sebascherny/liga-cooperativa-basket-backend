import { Game } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

const deleteGame = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteGame(id);
  return "Affected rows = "+affectedRows;
}

const gameDatasource = new GameDatasource();
export default deleteGame( gameDatasource );
