import { Game, UpdateGameInput } from '../../entities/Game';
import DataRepository from '../../repositories/game.repository';

import GameDatasource from '../../../dataSources/mysql/game.datasource';

import getCompetition from '../Competition/getCompetition.interactor';
import getBasketballCourt from '../BasketballCourt/getBasketballCourt.interactor';
import getTeam from '../Team/getTeam.interactor';

const updateGame = (dataRepository: DataRepository) => async ( updateGameInput :UpdateGameInput ): Promise<Game> => {

  let gameItem =await dataRepository.getGame(updateGameInput.id);

  if (updateGameInput.competitionId != null){
    let competition = await getCompetition(updateGameInput.competitionId);
    gameItem.competitionId = updateGameInput.competitionId;
  }
  if (updateGameInput.courtId != null){
    let court = await getBasketballCourt(updateGameInput.courtId);
    gameItem.courtId = updateGameInput.courtId;
  }
  if (updateGameInput.localId != null){
    let local = await getTeam(updateGameInput.localId);
    gameItem.localId = updateGameInput.localId;
  }
  if (updateGameInput.visitantId != null){
    let visitant = await getTeam(updateGameInput.visitantId);
    gameItem.visitantId = updateGameInput.visitantId;
  }
  if (updateGameInput.date != null){
    gameItem.date = updateGameInput.date;
  }
  if (updateGameInput.localScore != null){
    gameItem.localScore = updateGameInput.localScore;
  }
  if (updateGameInput.visitantScore != null){
    gameItem.visitantScore = updateGameInput.visitantScore;
  }
  if (updateGameInput.localFairPlay != null){
    gameItem.localFairPlay = updateGameInput.localFairPlay;
  }
  if (updateGameInput.visitantFairPlay != null){
    gameItem.visitantFairPlay = updateGameInput.visitantFairPlay;
  }
  if (updateGameInput.localMixed != null){
    gameItem.localMixed = updateGameInput.localMixed;
  }
  if (updateGameInput.visitantMixed != null){
    gameItem.visitantMixed = updateGameInput.visitantMixed;
  }
  if (updateGameInput.finished != null){
    gameItem.finished = updateGameInput.finished;
  }
  if (updateGameInput.confirmed != null){
    gameItem.confirmed = updateGameInput.confirmed;
  }

  let affectedRows = await dataRepository.updateGame(gameItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return gameItem;
}

const gameDatasource = new GameDatasource();
export default updateGame( gameDatasource );
