import {Competition} from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';

const deleteCompetition = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteCompetition(id);
  return "Affected rows = "+affectedRows;
}

const competitionDatasource = new CompetitionDatasource();
export default deleteCompetition( competitionDatasource );
