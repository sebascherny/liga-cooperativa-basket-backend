import { Competition, AddCompetitionInput } from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';

import getCompetitionGroup from '../CompetitionGroup/getCompetitionGroup.interactor';

const addCompetition = (dataRepository: DataRepository) => async ( addCompetitionInput :AddCompetitionInput ): Promise<Competition> => {

  let competitionGroup = await getCompetitionGroup(addCompetitionInput.competitionGroupId);

  let competitionItem = <Competition>{};
  competitionItem.competitionGroupId = competitionGroup.id;
  competitionItem.name = addCompetitionInput.name;

  let competitionId = await dataRepository.addCompetition(competitionItem);
  competitionItem.id = competitionId;

  return competitionItem;
}

const competitionDatasource = new CompetitionDatasource();
export default addCompetition( competitionDatasource );
