import {Competition, CompetitionExpanded} from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';
import getCompetitionGroup from '../CompetitionGroup/getCompetitionGroup.interactor';
import getCompetition from '../Competition/getCompetition.interactor';
import getLeague from '../League/getLeague.interactor';

const getCompetitionCompetitionExpanded = (dataRepository: DataRepository) => async ( id: number ): Promise<CompetitionExpanded> => {
  const competition: Competition = await getCompetition(id);

  let competitionExpandedItem = <CompetitionExpanded>{};

  competitionExpandedItem.id = competition.id;
  competitionExpandedItem.competitionGroup = await getCompetitionGroup(Number(competition.competitionGroupId));
  competitionExpandedItem.name = competition.name;

  return competitionExpandedItem;
}

const competitionDatasource = new CompetitionDatasource();
export default getCompetitionCompetitionExpanded( competitionDatasource );
