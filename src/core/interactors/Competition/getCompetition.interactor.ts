import {Competition} from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';

const getCompetition = (dataRepository: DataRepository) => async ( id: number ): Promise<Competition> => {
  const CompetitionItem: Competition = await dataRepository.getCompetition(id);
  return CompetitionItem;
}

const competitionDatasource = new CompetitionDatasource();
export default getCompetition( competitionDatasource );
