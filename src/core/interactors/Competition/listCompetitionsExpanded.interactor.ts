import {Competition, CompetitionExpanded, CompetitionFilters} from '../../entities/Competition';
import {CompetitionGroup} from '../../entities/CompetitionGroup';
import {League} from '../../entities/League';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';
import getCompetitionGroup from '../CompetitionGroup/getCompetitionGroup.interactor';
import getLeague from '../League/getLeague.interactor';

const listCompetitionsExpanded = (dataRepository: DataRepository) => async (filters: CompetitionFilters): Promise<CompetitionExpanded[]> => {
  const CompetitionItems: Competition[] = await dataRepository.listCompetitions(filters);

  let competitions: CompetitionExpanded[] = [];
  for (let competition of CompetitionItems) {

    let competitionExpandedItem = <CompetitionExpanded>{};

    competitionExpandedItem.id = competition.id;
    competitionExpandedItem.competitionGroup = await getCompetitionGroup(Number(competition.competitionGroupId));
    competitionExpandedItem.name = competition.name;

    competitions.push(competitionExpandedItem);
  }

  return competitions;
}

const competitionDatasource = new CompetitionDatasource();
export default listCompetitionsExpanded( competitionDatasource );
