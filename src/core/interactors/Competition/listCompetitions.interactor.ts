import {Competition, CompetitionFilters} from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';

const listCompetitions = (dataRepository: DataRepository) => async (filters: CompetitionFilters): Promise<Competition[]> => {
  const CompetitionItems: Competition[] = await dataRepository.listCompetitions(filters);
  return CompetitionItems;
}

const competitionDatasource = new CompetitionDatasource();
export default listCompetitions( competitionDatasource );
