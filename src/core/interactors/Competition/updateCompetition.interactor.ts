import { Competition, UpdateCompetitionInput } from '../../entities/Competition';
import DataRepository from '../../repositories/competition.repository';

import CompetitionDatasource from '../../../dataSources/mysql/competition.datasource';

import getCompetitionGroup from '../CompetitionGroup/getCompetitionGroup.interactor';

const updateCompetition = (dataRepository: DataRepository) => async ( updateCompetitionInput :UpdateCompetitionInput ): Promise<Competition> => {

  let competitionItem = await dataRepository.getCompetition(updateCompetitionInput.id);

  if (updateCompetitionInput.competitionGroupId != null){
    let competitionGroup = await getCompetitionGroup(updateCompetitionInput.competitionGroupId);
    competitionItem.competitionGroupId = competitionGroup.id;
  }
  if (updateCompetitionInput.name != null){
    competitionItem.name = updateCompetitionInput.name;
  }

  let affectedRows = await dataRepository.updateCompetition(competitionItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return competitionItem;
}

const competitionDatasource = new CompetitionDatasource();
export default updateCompetition( competitionDatasource );
