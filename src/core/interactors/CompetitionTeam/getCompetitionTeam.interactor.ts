import { CompetitionTeam } from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const getCompetitionTeam = (dataRepository: DataRepository) => async ( competitionId: number, teamId: number ): Promise<CompetitionTeam> => {
  const CompetitionTeamItem: CompetitionTeam = await dataRepository.getCompetitionTeam(competitionId, teamId);
  return CompetitionTeamItem;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getCompetitionTeam( competitionTeamDatasource );
