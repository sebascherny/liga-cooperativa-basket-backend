import {Team} from '../../entities/Team';
import DataRepository from '../../repositories/competitionTeam.repository';
import getTeam from '../Team/getTeam.interactor';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const getTeams = (dataRepository: DataRepository) => async ( competitionId: number ): Promise<Team[]> => {
  const TeamIdsItem: number[] = await dataRepository.getTeams(competitionId);

  let teams: Team[] = [];
  for (let teamId of TeamIdsItem) {
    const TeamItem: Team = await getTeam(teamId["team_id"]);

    teams.push(TeamItem);
  }

  return teams;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getTeams( competitionTeamDatasource );
