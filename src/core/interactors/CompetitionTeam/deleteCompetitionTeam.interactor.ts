import { CompetitionTeam } from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const deleteCompetitionTeam = (dataRepository: DataRepository) => async (
  competitionId: number,
  teamId: number,
) => {
  let competitionTeamId = await dataRepository.getCompetitionTeam(competitionId, teamId);

  let affectedRows = await dataRepository.deleteCompetitionTeam(competitionTeamId.id);

  return "Affected rows = "+affectedRows;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default deleteCompetitionTeam( competitionTeamDatasource );
