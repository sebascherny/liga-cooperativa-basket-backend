import { CompetitionTeam, UpdateCompetitionTeamInput } from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const updateCompetitionTeam = (dataRepository: DataRepository) => async ( updateCompetitionTeamInput :UpdateCompetitionTeamInput ): Promise<CompetitionTeam> => {

  let competitionTeamItem = await dataRepository.getCompetitionTeam(updateCompetitionTeamInput.competitionId, updateCompetitionTeamInput.teamId);
  if (updateCompetitionTeamInput.victories != null){
    competitionTeamItem.victories = updateCompetitionTeamInput.victories;
  }
  if (updateCompetitionTeamInput.defeats != null){
    competitionTeamItem.defeats = updateCompetitionTeamInput.defeats;
  }
  if (updateCompetitionTeamInput.pointsScored != null){
    competitionTeamItem.pointsScored = updateCompetitionTeamInput.pointsScored;
  }
  if (updateCompetitionTeamInput.pointsReceived != null){
    competitionTeamItem.pointsReceived = updateCompetitionTeamInput.pointsReceived;
  }
  if (updateCompetitionTeamInput.competitionPoints != null){
    competitionTeamItem.competitionPoints = updateCompetitionTeamInput.competitionPoints;
  }
  if (updateCompetitionTeamInput.fairPlay != null){
    competitionTeamItem.fairPlay = updateCompetitionTeamInput.fairPlay;
  }
  if (updateCompetitionTeamInput.mixedGames != null){
    competitionTeamItem.mixedGames = updateCompetitionTeamInput.mixedGames;
  }

  let affectedRows = await dataRepository.updateCompetitionTeam(competitionTeamItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return competitionTeamItem;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default updateCompetitionTeam( competitionTeamDatasource );
