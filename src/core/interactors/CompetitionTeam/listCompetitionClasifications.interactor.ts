import {CompetitionTeamExpanded} from '../../entities/CompetitionTeam';
import {CompetitionExpanded, CompetitionFilters} from '../../entities/Competition';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';
import getTeamsExpanded from './getTeamsExpanded.interactor';
import listCompetitionsExpanded from '../Competition/listCompetitionsExpanded.interactor';

const listCompetitionClasification = (dataRepository: DataRepository) => async (filters: CompetitionFilters): Promise<CompetitionTeamExpanded[][]> => {
  const Competitions: CompetitionExpanded[] = await listCompetitionsExpanded(filters);

  let competitionClasifications: CompetitionTeamExpanded[][] = [];
  for (let competition of Competitions) {
    let competitionTeamExpandedItems = await getTeamsExpanded(Number(competition.id));
    if (competitionTeamExpandedItems.length > 0){
      competitionClasifications.push(competitionTeamExpandedItems);
    }
  }

  return competitionClasifications;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default listCompetitionClasification( competitionTeamDatasource );
