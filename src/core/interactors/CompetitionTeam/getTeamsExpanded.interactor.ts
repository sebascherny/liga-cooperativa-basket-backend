import { CompetitionTeam, CompetitionTeamExpanded } from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';
import getTeam from '../Team/getTeam.interactor';
import getCompetitionExpanded from '../Competition/getCompetitionExpanded.interactor';

const getTeamsExpanded = (dataRepository: DataRepository) => async ( competitionId: number ): Promise<CompetitionTeamExpanded[]> => {
  const TeamIdsItem: number[] = await dataRepository.getTeamsOrdered(competitionId);

  let teams: CompetitionTeamExpanded[] = [];
  for (let teamId of TeamIdsItem) {
    const CompetitionTeamItem: CompetitionTeam = await dataRepository.getCompetitionTeam(competitionId, teamId["team_id"]);

    let competitionTeamExpandedItem = <CompetitionTeamExpanded>{};
    competitionTeamExpandedItem.id = CompetitionTeamItem.id;
    competitionTeamExpandedItem.competition = await getCompetitionExpanded(Number(CompetitionTeamItem.competitionId));
    competitionTeamExpandedItem.team = await getTeam(Number(CompetitionTeamItem.teamId));
    competitionTeamExpandedItem.victories = CompetitionTeamItem.victories;
    competitionTeamExpandedItem.defeats = CompetitionTeamItem.defeats;
    competitionTeamExpandedItem.pointsScored = CompetitionTeamItem.pointsScored;
    competitionTeamExpandedItem.pointsReceived = CompetitionTeamItem.pointsReceived;
    competitionTeamExpandedItem.competitionPoints = CompetitionTeamItem.competitionPoints;
    competitionTeamExpandedItem.fairPlay = CompetitionTeamItem.fairPlay;
    competitionTeamExpandedItem.mixedGames = CompetitionTeamItem.mixedGames;

    teams.push(competitionTeamExpandedItem);
  }

  return teams;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getTeamsExpanded( competitionTeamDatasource );
