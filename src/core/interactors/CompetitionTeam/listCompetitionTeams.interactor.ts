import {CompetitionTeam} from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const listCompetitionTeams = (dataRepository: DataRepository) => async (): Promise<CompetitionTeam[]> => {
  const CompetitionTeamItems: CompetitionTeam[] = await dataRepository.listCompetitionTeams();
  return CompetitionTeamItems;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default listCompetitionTeams( competitionTeamDatasource );
