import {CompetitionExpanded} from '../../entities/Competition';
import DataRepository from '../../repositories/competitionTeam.repository';
import getCompetitionExpanded from '../Competition/getCompetitionExpanded.interactor';
import getCompetitions from './getCompetitions.interactor';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const getActiveCompetitions = (dataRepository: DataRepository) => async ( teamId: number ): Promise<CompetitionExpanded[]> => {
  const CompetitionIdsItem: number[] = await getCompetitions(teamId);

  let competitions: CompetitionExpanded[] = [];
  for (let id of CompetitionIdsItem) {
    let competition = await getCompetitionExpanded(id)
    if (competition.competitionGroup.active){
      competitions.push(competition);
    }
  }
  return competitions;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getActiveCompetitions( competitionTeamDatasource );
