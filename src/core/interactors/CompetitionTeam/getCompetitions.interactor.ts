import {Competition} from '../../entities/Competition';
import DataRepository from '../../repositories/competitionTeam.repository';
import getCompetition from '../Competition/getCompetition.interactor';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const getCompetitions = (dataRepository: DataRepository) => async ( teamId: number ): Promise<number[]> => {
  const CompetitionIdsItem: number[] = await dataRepository.getCompetitions(teamId);

  let competitionIds: number[] = [];
  for (let id of CompetitionIdsItem) {
    competitionIds.push(id["competition_id"]);
  }

  return competitionIds;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getCompetitions( competitionTeamDatasource );
