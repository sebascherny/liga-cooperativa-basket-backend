import { CompetitionTeam, AddCompetitionTeamInput, CompetitionTeamDefaults } from '../../entities/CompetitionTeam';
import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

import getCompetition from '../Competition/getCompetition.interactor';
import getTeam from '../Team/getTeam.interactor';

const addCompetitionTeam = (dataRepository: DataRepository) => async ( addCompetitionTeamInput :AddCompetitionTeamInput ): Promise<CompetitionTeam> => {

  let competition = await getCompetition(addCompetitionTeamInput.competitionId);
  let team = await getTeam(addCompetitionTeamInput.teamId);

  let competitionTeamItem = <CompetitionTeam>{};
  competitionTeamItem.competitionId = addCompetitionTeamInput.competitionId;
  competitionTeamItem.teamId = addCompetitionTeamInput.teamId;

  competitionTeamItem.victories = addCompetitionTeamInput.victories;
  if (addCompetitionTeamInput.victories == null) {
    addCompetitionTeamInput.victories = CompetitionTeamDefaults.victories;
  }

  competitionTeamItem.defeats = addCompetitionTeamInput.defeats;
  if (addCompetitionTeamInput.defeats == null) {
    addCompetitionTeamInput.defeats = CompetitionTeamDefaults.defeats;
  }

  competitionTeamItem.pointsScored = addCompetitionTeamInput.pointsScored;
  if (addCompetitionTeamInput.pointsScored == null) {
    competitionTeamItem.pointsScored = CompetitionTeamDefaults.pointsScored;
  }

  competitionTeamItem.pointsReceived = addCompetitionTeamInput.pointsReceived;
  if (addCompetitionTeamInput.pointsReceived == null) {
    competitionTeamItem.pointsReceived = CompetitionTeamDefaults.pointsReceived;
  }

  competitionTeamItem.competitionPoints = addCompetitionTeamInput.competitionPoints;
  if (addCompetitionTeamInput.competitionPoints == null) {
    competitionTeamItem.competitionPoints = CompetitionTeamDefaults.competitionPoints;
  }

  competitionTeamItem.fairPlay = addCompetitionTeamInput.fairPlay;
  if (addCompetitionTeamInput.fairPlay == null) {
    competitionTeamItem.fairPlay = CompetitionTeamDefaults.fairPlay;
  }

  competitionTeamItem.mixedGames = addCompetitionTeamInput.mixedGames;
  if (addCompetitionTeamInput.mixedGames == null) {
    competitionTeamItem.mixedGames = CompetitionTeamDefaults.mixedGames;
  }

  let competitionTeamId = await dataRepository.addCompetitionTeam(competitionTeamItem);
  competitionTeamItem.id = competitionTeamId;
  return competitionTeamItem;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default addCompetitionTeam( competitionTeamDatasource );
