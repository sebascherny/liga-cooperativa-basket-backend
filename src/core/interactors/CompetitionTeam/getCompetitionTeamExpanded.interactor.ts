import { CompetitionTeam, CompetitionTeamExpanded } from '../../entities/CompetitionTeam';
import {Team} from '../../entities/Team';

import getTeam from '../Team/getTeam.interactor';
import getCompetitionExpanded from '../Competition/getCompetitionExpanded.interactor';

import DataRepository from '../../repositories/competitionTeam.repository';

import CompetitionTeamDatasource from '../../../dataSources/mysql/competitionTeam.datasource';

const getCompetitionTeamExpanded = (dataRepository: DataRepository) => async ( competitionId: number, teamId: number ): Promise<CompetitionTeamExpanded> => {

  const CompetitionTeamItem: CompetitionTeam = await dataRepository.getCompetitionTeam(competitionId, teamId);

  let competitionTeamExpandedItem = <CompetitionTeamExpanded>{};
  competitionTeamExpandedItem.id = CompetitionTeamItem.id;
  competitionTeamExpandedItem.competition = await getCompetitionExpanded(Number(CompetitionTeamItem.competitionId));
  competitionTeamExpandedItem.team = await getTeam(Number(CompetitionTeamItem.teamId));
  competitionTeamExpandedItem.victories = CompetitionTeamItem.victories;
  competitionTeamExpandedItem.defeats = CompetitionTeamItem.defeats;
  competitionTeamExpandedItem.pointsScored = CompetitionTeamItem.pointsScored;
  competitionTeamExpandedItem.pointsReceived = CompetitionTeamItem.pointsReceived;
  competitionTeamExpandedItem.competitionPoints = CompetitionTeamItem.competitionPoints;
  competitionTeamExpandedItem.fairPlay = CompetitionTeamItem.fairPlay;
  competitionTeamExpandedItem.mixedGames = CompetitionTeamItem.mixedGames;

  return competitionTeamExpandedItem;
}

const competitionTeamDatasource = new CompetitionTeamDatasource();
export default getCompetitionTeamExpanded( competitionTeamDatasource );
