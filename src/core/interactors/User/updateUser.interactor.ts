const bcrypt = require('bcrypt');

import { User, UpdateUserInput } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';
import UserDatasource from '../../../dataSources/mysql/user.datasource';
import getTeam from '../Team/getTeam.interactor';

const updateUser = (dataRepository: DataRepository) => async ( updateUserInput :UpdateUserInput ): Promise<User> => {

  let userItem = await dataRepository.getUser(updateUserInput.id);

  if (updateUserInput.teamId != null){
    let team = await getTeam(updateUserInput.teamId);
    userItem.teamId = updateUserInput.teamId;
  }
  if (updateUserInput.name != null){
    if (updateUserInput.name.includes(" ")){
      throw Error("Name can not contain empty characters");
    }

    userItem.name = updateUserInput.name;
  }
  if (updateUserInput.password != null){
    const hash = bcrypt.hashSync(updateUserInput.password, Number(process.env.SALT));
    userItem.password = hash;
  }
  if (updateUserInput.email != null){
    userItem.email = updateUserInput.email;
  }
  if (updateUserInput.admin != null){
    userItem.admin = updateUserInput.admin;
  }

  let affectedRows = await dataRepository.updateUser(userItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return userItem;
}

const userDatasource = new UserDatasource();
export default updateUser( userDatasource );
