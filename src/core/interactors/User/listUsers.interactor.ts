import { User, UserFilters } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';

import UserDatasource from '../../../dataSources/mysql/user.datasource';

const listUsers = (dataRepository: DataRepository) => async (filters: UserFilters): Promise<User[]> => {
  const UserItems: User[] = await dataRepository.listUsers(filters);
  return UserItems;
}

const userDatasource = new UserDatasource();
export default listUsers( userDatasource );
