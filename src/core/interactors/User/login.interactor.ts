const bcrypt = require('bcrypt');
import jwt, { Secret, JwtPayload } from 'jsonwebtoken';

import LoginResponse from '../../entities/LoginResponse';
import { User, UserFilters } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';
import UserDatasource from '../../../dataSources/mysql/user.datasource';

const login = (dataRepository: DataRepository) => async (input: UserFilters): Promise<LoginResponse> => {

  let userFilters = <UserFilters>{};
  userFilters.name = input.name;
  const UserItems: User[] = await dataRepository.listUsers(userFilters);

  if ( UserItems.length == 0 ){
    throw Error("Wrong credentials");
  }

  if ( UserItems.length >> 1 ){
    throw Error("Wrong credentials");
  }

  if ( !bcrypt.compareSync(input.password, UserItems[0].password) ) {
    throw Error("Wrong credentials");
  }

  const token = jwt.sign({ _id: UserItems[0].id?.toString(), name: UserItems[0].name }, process.env.SECRET_KEY, {
    expiresIn: '200 days',
  });

  let loginResponse = <LoginResponse>{};
  loginResponse.userId = UserItems[0].id;
  loginResponse.teamId = UserItems[0].teamId;
  loginResponse.admin = UserItems[0].admin;
  loginResponse.token = token;

  return loginResponse;
}

const userDatasource = new UserDatasource();
export default login( userDatasource );
