const bcrypt = require('bcrypt');

import { User, UpdatePasswordInput } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';
import UserDatasource from '../../../dataSources/mysql/user.datasource';
import getTeam from '../Team/getTeam.interactor';

const updatePassword = (dataRepository: DataRepository) => async ( updatePasswordInput :UpdatePasswordInput ): Promise<User> => {

  let userItem = await dataRepository.getUser(updatePasswordInput.id);


  if ( !bcrypt.compareSync(updatePasswordInput.oldPassword, userItem.password) ) {
    throw Error("Wrong password");
  }

  if (updatePasswordInput.newPassword != null){
    const hash = bcrypt.hashSync(updatePasswordInput.newPassword, Number(process.env.SALT));
    userItem.password = hash;
  }

  let affectedRows = await dataRepository.updateUser(userItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return userItem;
}

const userDatasource = new UserDatasource();
export default updatePassword( userDatasource );
