import { User } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';

import UserDatasource from '../../../dataSources/mysql/user.datasource';

const deleteUser = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteUser(id);
  return "Affected rows = "+affectedRows;
}

const userDatasource = new UserDatasource();
export default deleteUser( userDatasource );
