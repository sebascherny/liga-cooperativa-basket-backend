const bcrypt = require('bcrypt');

import { User, AddUserInput } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';
import UserDatasource from '../../../dataSources/mysql/user.datasource';
import getTeam from '../Team/getTeam.interactor';

const addUser = (dataRepository: DataRepository) => async ( addUserInput :AddUserInput ): Promise<User> => {

  if (addUserInput.name.includes(" ")){
    throw Error("Name can not contain empty characters");
  }

  let team = await getTeam(addUserInput.teamId);

  let userItem = <User>{};
  userItem.teamId = addUserInput.teamId;
  userItem.name = addUserInput.name;

  const hash = bcrypt.hashSync(addUserInput.password, Number(process.env.SALT));
  userItem.password = hash;

  userItem.email = addUserInput.email;
  userItem.admin = addUserInput.admin;

  let userId = await dataRepository.addUser(userItem);
  userItem.id = userId;

  return userItem;

}

const userDatasource = new UserDatasource();
export default addUser( userDatasource );
