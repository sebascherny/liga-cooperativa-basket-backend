import { User } from '../../entities/User';
import DataRepository from '../../repositories/user.repository';

import UserDatasource from '../../../dataSources/mysql/user.datasource';

const getUser = (dataRepository: DataRepository) => async ( id: number ): Promise<User> => {
  const UserItem: User = await dataRepository.getUser(id);
  return UserItem;
}

const userDatasource = new UserDatasource();
export default getUser( userDatasource );
