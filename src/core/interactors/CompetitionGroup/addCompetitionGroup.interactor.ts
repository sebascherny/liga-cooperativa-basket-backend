import { CompetitionGroup, AddCompetitionGroupInput } from '../../entities/CompetitionGroup';
import DataRepository from '../../repositories/competitionGroup.repository';

import CompetitionGroupDatasource from '../../../dataSources/mysql/competitionGroup.datasource';

import getLeague from '../League/getLeague.interactor';

const addCompetitionGroup = (dataRepository: DataRepository) => async ( addCompetitionGroupInput :AddCompetitionGroupInput): Promise<CompetitionGroup> => {

  let league = await getLeague(addCompetitionGroupInput.leagueId);

  let competitionGroupItem = <CompetitionGroup>{};
  competitionGroupItem.leagueId = league.id;
  competitionGroupItem.name = addCompetitionGroupInput.name;
  competitionGroupItem.startDate = addCompetitionGroupInput.startDate;
  competitionGroupItem.endDate = addCompetitionGroupInput.endDate;
  competitionGroupItem.active = addCompetitionGroupInput.active;

  let competitionGroupId = await dataRepository.addCompetitionGroup(competitionGroupItem);
  competitionGroupItem.id = competitionGroupId;

  return competitionGroupItem;
}

const competitionGroupDatasource = new CompetitionGroupDatasource();
export default addCompetitionGroup( competitionGroupDatasource );
