import {CompetitionGroup} from '../../entities/CompetitionGroup';
import DataRepository from '../../repositories/competitionGroup.repository';

import CompetitionGroupDatasource from '../../../dataSources/mysql/competitionGroup.datasource';

const getCompetitionGroup = (dataRepository: DataRepository) => async ( id: number ): Promise<CompetitionGroup> => {
  const CompetitionGroupItem: CompetitionGroup = await dataRepository.getCompetitionGroup(id);
  return CompetitionGroupItem;
}

const competitionGroupDatasource = new CompetitionGroupDatasource();
export default getCompetitionGroup( competitionGroupDatasource );
