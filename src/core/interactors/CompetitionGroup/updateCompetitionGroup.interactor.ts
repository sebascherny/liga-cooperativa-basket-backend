import { CompetitionGroup, UpdateCompetitionGroupInput } from '../../entities/CompetitionGroup';
import DataRepository from '../../repositories/competitionGroup.repository';

import CompetitionGroupDatasource from '../../../dataSources/mysql/competitionGroup.datasource';

import getLeague from '../League/getLeague.interactor';

const updateCompetitionGroup = (dataRepository: DataRepository) => async ( updateCompetitionGroupInput :UpdateCompetitionGroupInput ): Promise<CompetitionGroup> => {

  let competitionGroupItem = await dataRepository.getCompetitionGroup(updateCompetitionGroupInput.id);

  if (updateCompetitionGroupInput.leagueId != null){
    let league = await getLeague(updateCompetitionGroupInput.leagueId);
    competitionGroupItem.leagueId = league.id;
  }
  if (updateCompetitionGroupInput.name != null){
    competitionGroupItem.name = updateCompetitionGroupInput.name;
  }
  if (updateCompetitionGroupInput.startDate != null){
    competitionGroupItem.startDate = updateCompetitionGroupInput.startDate;
  }
  if (updateCompetitionGroupInput.endDate != null){
    competitionGroupItem.endDate = updateCompetitionGroupInput.endDate;
  }
  if (updateCompetitionGroupInput.active != null){
    competitionGroupItem.active = updateCompetitionGroupInput.active;
  }

  let affectedRows = await dataRepository.updateCompetitionGroup(competitionGroupItem);

  if (affectedRows == 0) {
      throw new Error('No rows updated');
  }

  return competitionGroupItem;
}

const competitionGroupDatasource = new CompetitionGroupDatasource();
export default updateCompetitionGroup( competitionGroupDatasource );
