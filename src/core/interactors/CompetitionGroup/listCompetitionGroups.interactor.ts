import {CompetitionGroup, CompetitionGroupFilters} from '../../entities/CompetitionGroup';
import DataRepository from '../../repositories/competitionGroup.repository';

import CompetitionGroupDatasource from '../../../dataSources/mysql/competitionGroup.datasource';

const listCompetitionGroups = (dataRepository: DataRepository) => async (filters: CompetitionGroupFilters): Promise<CompetitionGroup[]> => {
  const CompetitionGroupItems: CompetitionGroup[] = await dataRepository.listCompetitionGroups(filters);
  return CompetitionGroupItems;
}

const competitionGroupDatasource = new CompetitionGroupDatasource();
export default listCompetitionGroups( competitionGroupDatasource );
