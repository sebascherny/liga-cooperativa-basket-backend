import {CompetitionGroup} from '../../entities/CompetitionGroup';
import DataRepository from '../../repositories/competitionGroup.repository';

import CompetitionGroupDatasource from '../../../dataSources/mysql/competitionGroup.datasource';

const deleteCompetitionGroup = (dataRepository: DataRepository) => async (
  id: number
) => {
  let affectedRows = await dataRepository.deleteCompetitionGroup(id);
  return "Affected rows = "+affectedRows;
}

const competitionGroupDatasource = new CompetitionGroupDatasource();
export default deleteCompetitionGroup( competitionGroupDatasource );
