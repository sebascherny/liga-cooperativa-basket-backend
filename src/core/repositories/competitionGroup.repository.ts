import {CompetitionGroup, CompetitionGroupFilters} from '../entities/CompetitionGroup';

export default interface CompetitionGroupRepository {

  addCompetitionGroup(competitionGroupItem: CompetitionGroup): Promise<number>;

  getCompetitionGroup(id: number): Promise<CompetitionGroup>;

  listCompetitionGroups(filters :CompetitionGroupFilters): Promise<CompetitionGroup[]>;

  updateCompetitionGroup(competitionGroupItem: CompetitionGroup): Promise<number>;

  deleteCompetitionGroup(id: number): Promise<number>;

}
