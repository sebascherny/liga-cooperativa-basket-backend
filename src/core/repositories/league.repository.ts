import {League} from '../entities/League';

export default interface LeagueRepository {

  addLeague(leagueItem: League): Promise<number>;

  getLeague(id: number): Promise<League>;

  listLeagues(): Promise<League[]>;

  updateLeague(leagueItem: League): Promise<number>;

  deleteLeague(id: number): Promise<number>;

}
