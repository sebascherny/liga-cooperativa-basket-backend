import { Game, GameFilters } from '../entities/Game';

export default interface GameRepository {

  addGame(game: Game): Promise<number>;

  getGame(id: number): Promise<Game>;

  listGames(filters :GameFilters): Promise<Game[]>;

  updateGame(game: Game): Promise<number>;

  addGameResult(game: Game): Promise<number>;

  addGameFeedback(game: Game): Promise<number>;

  deleteGame(id: number): Promise<number>;
}
