import { User, UserFilters } from '../entities/User';

export default interface UserRepository {

  addUser(user: User): Promise<number>;

  getUser(id: number): Promise<User>;

  listUsers(filters: UserFilters): Promise<User[]>;

  updateUser(user: User): Promise<number>;

  deleteUser(id: number): Promise<number>;
}
