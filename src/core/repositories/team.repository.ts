import {Team} from '../entities/Team';

export default interface TeamRepository {

  addTeam(team: Team): Promise<number>;

  getTeam(id: number): Promise<Team>;

  listTeams(): Promise<Team[]>;

  updateTeam(team: Team): Promise<number>;

  deleteTeam(id: number): Promise<number>;
}
