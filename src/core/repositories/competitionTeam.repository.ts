import {Competition} from '../entities/Competition';
import {Team} from '../entities/Team';
import {CompetitionTeam} from '../entities/CompetitionTeam';

export default interface CompetitionTeamRepository {

  addCompetitionTeam(competitionTeamItem: CompetitionTeam): Promise<number>;

  getTeams(competitionId: number): Promise<number[]>;

  listCompetitionTeams(): Promise<CompetitionTeam[]>;

  getTeamsOrdered(competitionId: number): Promise<number[]>;

  getCompetitions(teamId: number): Promise<number[]>;

  getCompetitionTeam(competitionId: number, teamId: number): Promise<CompetitionTeam>;

  updateCompetitionTeam(competitionTeam: CompetitionTeam): Promise<number>;

  deleteCompetitionTeam(id: number): Promise<number>;

}
