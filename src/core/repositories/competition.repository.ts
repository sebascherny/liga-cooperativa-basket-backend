import {Competition, CompetitionFilters} from '../entities/Competition';

export default interface CompetitionRepository {

  addCompetition(competition: Competition): Promise<number>;

  getCompetition(id: number): Promise<Competition>;

  listCompetitions(filters :CompetitionFilters): Promise<Competition[]>;

  updateCompetition(competition: Competition): Promise<number>;

  deleteCompetition(id: number): Promise<number>;
}
