import {BasketballCourt} from '../entities/BasketballCourt';

export default interface BasketballCourtRepository {

  addBasketballCourt(basketballCourt: BasketballCourt): Promise<number>;

  getBasketballCourt(id: number): Promise<BasketballCourt>;

  listBasketballCourts(): Promise<BasketballCourt[]>;

  updateBasketballCourt(basketballCourt: BasketballCourt): Promise<number>;

  deleteBasketballCourt(id: number): Promise<number>;
}
