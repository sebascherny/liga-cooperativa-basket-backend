export interface BasketballCourt {
  id: number;
  name: string;
  direction: string;
  status: string;
  lights: boolean;
  lightTimeout: string;
}

export interface AddBasketballCourtInput {
  name: string;
  direction: string;
  status: string;
  lights: boolean;
  lightTimeout: string;
}

export interface UpdateBasketballCourtInput {
  id: number;
  name: string;
  direction: string;
  status: string;
  lights: boolean;
  lightTimeout: string;
}
