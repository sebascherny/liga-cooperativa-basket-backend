import {CompetitionGroup} from './CompetitionGroup';

export interface Competition {
  id: number;
  competitionGroupId: number;
  name: string;
}

export interface CompetitionExpanded {
  id: number;
  competitionGroup: CompetitionGroup;
  name: string;
}

export interface AddCompetitionInput {
  competitionGroupId: number;
  name: string;
}

export interface UpdateCompetitionInput {
  id: number;
  competitionGroupId: number;
  name: string;
}

export interface CompetitionFilters {
  competitionGroupId: string;
}
