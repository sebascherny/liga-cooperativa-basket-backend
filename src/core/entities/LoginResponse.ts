import {Competition} from './Competition';

export default interface LoginResponse {
  userId: number;
  teamId: number;
  admin: boolean;
  competitions: Competition[];
  token: string;
  firstCompetitionPendingGames: boolean;
  firstCompetitionPendingActions: boolean;
}
