import {CompetitionExpanded} from './Competition';
import {Team} from './Team';

export const CompetitionTeamDefaults = {
  victories: 0,
  defeats: 0,
  pointsScored: 0,
  pointsReceived: 0,
  competitionPoints: 0,
  fairPlay: 0,
  mixedGames: 0
} as const;

export interface CompetitionTeam {
  id: number;
  competitionId: number;
  teamId: number;
  victories: number;
  defeats: number;
  pointsScored: number;
  pointsReceived: number;
  competitionPoints: number;
  fairPlay: number;
  mixedGames: number;
}

export interface AddCompetitionTeamInput {
  competitionId: number;
  teamId: number;
  victories: number;
  defeats: number;
  pointsScored: number;
  pointsReceived: number;
  competitionPoints: number;
  fairPlay: number;
  mixedGames: number;
}

export interface UpdateCompetitionTeamInput {
  id: number;
  competitionId: number;
  teamId: number;
  victories: number;
  defeats: number;
  pointsScored: number;
  pointsReceived: number;
  competitionPoints: number;
  fairPlay: number;
  mixedGames: number;
}

export interface CompetitionTeamExpanded {
  id: number;
  competition: CompetitionExpanded;
  team: Team;
  victories: number;
  defeats: number;
  pointsScored: number;
  pointsReceived: number;
  competitionPoints: number;
  fairPlay: number;
  mixedGames: number;
}
