export interface User {
  id: number;
  teamId: number;
  name: string;
  password: string;
  email: string;
  admin: boolean;
}

export interface AddUserInput {
  id: number;
  teamId: number;
  name: string;
  password: string;
  email: string;
  admin: boolean;
}

export interface UpdateUserInput {
  id: number;
  teamId: number;
  name: string;
  password: string;
  email: string;
  admin: boolean;
}

export interface UpdatePasswordInput {
  id: number;
  oldPassword: string;
  newPassword: string;
}

export interface UserFilters {
  teamId: string;
  name: string;
  password: string;
  email: string;
}
