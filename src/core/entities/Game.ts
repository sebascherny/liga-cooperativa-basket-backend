import {Competition} from './Competition';
import {BasketballCourt} from './BasketballCourt';
import {Team} from './Team';

export const GameDefaults = {
  localScore: 0,
  visitantScore: 0,
  localFairPlay: 0,
  visitantFairPlay: 0,
  localMixed: false,
  visitantMixed: false,
  finished: false,
  confirmed: false,
  victoryPoints: 2,
  defeatPoints: 1,
  gameDuration: 74
} as const;

export interface Game {
  id: number;
  competitionId: number;
  courtId: number;
  localId: number;
  visitantId: number;
  date: Date;
  localScore: number;
  visitantScore: number;
  localFairPlay: number;
  visitantFairPlay: number;
  localMixed: boolean;
  visitantMixed: boolean;
  finished: boolean;
  confirmed: boolean;
}

export interface AddGameInput {
  competitionId: number;
  courtId: number;
  localId: number;
  visitantId: number;
  date: Date;
  localScore: number;
  visitantScore: number;
  localFairPlay: number;
  visitantFairPlay: number;
  localMixed: boolean;
  visitantMixed: boolean;
  finished: boolean;
  confirmed: boolean;
}

export interface UpdateGameInput {
  id: number;
  competitionId: number;
  courtId: number;
  localId: number;
  visitantId: number;
  date: Date;
  localScore: number;
  visitantScore: number;
  localFairPlay: number;
  visitantFairPlay: number;
  localMixed: boolean;
  visitantMixed: boolean;
  finished: boolean;
  confirmed: boolean;
}

export interface AddGameFeedbackInput {
  id: number;
  localFairPlay: number;
  localMixed: boolean;
}

export interface AddGameResultInput {
  id: number;
  localScore: number;
  visitantScore: number;
  visitantFairPlay: number;
  visitantMixed: boolean;
}

export interface CreateGameInput {
  competitionId: number;
  courtId: number;
  localId: number;
  visitantId: number;
  date: Date;
}

export interface GameExpanded {
  id: number;
  competition: Competition;
  court: BasketballCourt;
  local: Team;
  visitant: Team;
  date: Date;
  localScore: number;
  visitantScore: number;
  localMixed: boolean;
  visitantMixed: boolean;
  finished: boolean;
  confirmed: boolean;
}

export interface GameFilters {
  competitionId: string;
  courtId: string;
  teamId: string;
  localId: string;
  visitantId: string;
  startDate: string;
  endDate: string;
  finished: string;
  confirmed: string;
}
