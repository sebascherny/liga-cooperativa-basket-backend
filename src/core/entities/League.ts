export interface League {
  id: number;
  name: string;
  year: string;
}

export interface AddLeagueInput {
  name: string;
  year: string;
}

export interface UpdateLeagueInput {
  id: number;
  name: string;
  year: string;
}
