
import {League} from './League';

export interface CompetitionGroup {
  id: number;
  leagueId: number;
  name: string;
  startDate: Date;
  endDate: Date;
  active: boolean;
}

export interface CompetitionGroupExpanded {
  id: number;
  league: League;
  name: string;
  startDate: Date;
  endDate: Date;
  active: boolean;
}

export interface AddCompetitionGroupInput {
  leagueId: number;
  name: string;
  startDate: Date;
  endDate: Date;
  active: boolean;
}

export interface UpdateCompetitionGroupInput {
  id: number;
  leagueId: number;
  name: string;
  startDate: Date;
  endDate: Date;
  active: boolean;
}

export interface CompetitionGroupFilters {
  leagueId: string;
  active: string;
}
