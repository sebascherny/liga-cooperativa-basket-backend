import MySqlConnection from './mysql.connection';

import LeagueRepository from '../../core/repositories/league.repository';
import {League} from '../../core/entities/League';

export default class LeagueDatasource implements LeagueRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

    addLeague = (league: League): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = `INSERT INTO league (name, year) VALUES
            (?,?)`;

            let values = [league.name,league.year]

            con.query(sql, values, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.insertId);
            });
        });
    };

    getLeague = (id: number): Promise<League> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT * FROM league WHERE id=?";

            con.query(sql, id, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                if(result[0] == null) {
                  return reject(Error("League with id="+id+" not found"));
                }

                let league:League = {
                  id: result[0].id,
                  name: result[0].name,
                  year: result[0].year,
                };

                return resolve(league);
            });
        });
    };

    listLeagues = (): Promise<League[]> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT * FROM league";

            con.query(sql, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                let leagues:League[] = [];
                result.forEach( (element) => {
                  let league:League = {
                    id: element.id,
                    name: element.name,
                    year: element.year,
                  };
                  leagues.push(league)
                })

                return resolve(leagues);
            });
        });
    };

    updateLeague = (league: League): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = `UPDATE league SET name = ?, year = ? WHERE id = ?`;

            let values = [league.name,league.year,league.id]

            con.query(sql, values, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.affectedRows);
            });
        });
    };

    deleteLeague = (id: number): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "DELETE FROM league WHERE id =?";


            con.query(sql, id, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.affectedRows);
            });
        });
    };

}
