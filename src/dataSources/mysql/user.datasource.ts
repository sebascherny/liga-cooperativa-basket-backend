import MySqlConnection from './mysql.connection';

import UserRepository from '../../core/repositories/user.repository';
import type { User, UserFilters } from '../../core/entities/User';

export default class UserDatasource implements UserRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addUser = (user: User): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `INSERT INTO user
          ( team_id,
            name,
            password,
            email,
            admin )
          VALUES (?,?,?,?,?)`;

          let values = [user.teamId,
                        user.name,
                        user.password,
                        user.email,
                        +!!user.admin]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getUser = (id: number): Promise<User> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM user WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("User with id="+id+" not found"));
              }

              let user:User = {
                id: result[0].id,
                teamId: result[0].team_id,
                name: result[0].name,
                password: result[0].password,
                email: result[0].email,
                admin: !!result[0].admin,
              };

              return resolve(user);
          });
      });
  };

  listUsers = (filters: UserFilters): Promise<User[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM user WHERE 1=1 ";

          if (filters.teamId != null) {
            sql = sql + `AND team_id = ${filters.teamId} `;
          }

          if (filters.name != null) {
            sql = sql + `AND name = '${filters.name}' `;
          }

          if (filters.password != null) {
            sql = sql + `AND password = '${filters.password}' `;
          }

          if (filters.email != null) {
            sql = sql + `AND email = ${filters.email} `;
          }

          con.query(sql, (error, result)=>{
              let users:User[] = [];
              if (result != undefined ){
                result.forEach( (element) => {
                  let user:User = {
                    id: element.id,
                    teamId: element.team_id,
                    name: element.name,
                    password: element.password,
                    email: element.email,
                    admin: !!element.admin,
                  };
                  users.push(user)
                })
              }

              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }
              return resolve(users);
          });
      });
  };

  updateUser = (user: User): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE user SET
          team_id = ?,
          name = ?,
          password = ?,
          email = ?,
          admin = ? WHERE id = ?`;

          let values = [user.teamId,
                        user.name,
                        user.password,
                        user.email,
                        +!!user.admin,
                        user.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteUser = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM user WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
