import MySqlConnection from './mysql.connection';

import GameRepository from '../../core/repositories/game.repository';
import { Game, GameFilters} from '../../core/entities/Game';

export default class GameDatasource implements GameRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addGame = (game: Game): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `INSERT INTO game (
            competition_id,
            court_id,
            local_id,
            visitant_id,
            date,
            local_score,
            visitant_score,
            local_fair_play,
            visitant_fair_play,
            local_mixed,
            visitant_mixed,
            finished,
            confirmed) VALUES
          (?,?,?,?,?,?,?,?,?,?,?,?,?)`;

          let values = [game.competitionId,
                        game.courtId,
                        game.localId,
                        game.visitantId,
                        game.date,
                        game.localScore,
                        game.visitantScore,
                        game.localFairPlay,
                        game.visitantFairPlay,
                        +!!game.localMixed,
                        +!!game.visitantMixed,
                        +!!game.finished,
                        +!!game.confirmed]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getGame = (id: number): Promise<Game> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM game WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("Game with id="+id+" not found"));
              }

              var userTimezoneOffset = result[0].date.getTimezoneOffset() * 60000;
              let newDate = new Date(result[0].date.getTime() - userTimezoneOffset);

              let game:Game = {
                id: result[0].id,
                competitionId: result[0].competition_id,
                courtId: result[0].court_id,
                localId: result[0].local_id,
                visitantId: result[0].visitant_id,
                date: newDate,
                localScore: result[0].local_score,
                visitantScore: result[0].visitant_score,
                localFairPlay: result[0].local_fair_play,
                visitantFairPlay: result[0].visitant_fair_play,
                localMixed: !!result[0].local_mixed,
                visitantMixed: !!result[0].visitant_mixed,
                finished: !!result[0].finished,
                confirmed: !!result[0].confirmed,
              };

              return resolve(game);
          });
      });
  };

  listGames = (filters: GameFilters): Promise<Game[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM game WHERE 1=1 ";

          if (filters.competitionId != null) {
            sql = sql + `AND competition_id = ${filters.competitionId} `;
          }
          if (filters.courtId != null) {
            sql = sql + `AND court_id = ${filters.courtId} `;
          }
          if (filters.teamId != null) {
              sql = sql + `AND ( local_id = ${filters.teamId} OR visitant_id = ${filters.teamId}) `;
          } else {
            if (filters.localId != null) {
              sql = sql + `AND local_id = ${filters.localId} `;
            }
            if (filters.visitantId != null) {
              sql = sql + `AND visitant_id = ${filters.visitantId} `;
            }
          }
          if (filters.startDate != null) {
            sql = sql + `AND date >= STR_TO_DATE('${filters.startDate}', '%Y-%m-%d %H:%i:%s') `;
          }
          if (filters.endDate != null) {
            sql = sql + `AND date <= STR_TO_DATE('${filters.endDate}', '%Y-%m-%d %H:%i:%s') `;
          }
          if (filters.finished != null) {
            if (filters.finished.toLowerCase() == 'true') {
              sql = sql + `AND finished = 1 `;
            } else {
              sql = sql + `AND finished = 0 `;
            }
          }

          if (filters.confirmed != null) {
            if (filters.confirmed.toLowerCase() == 'true') {
              sql = sql + `AND confirmed = 1 `;
            } else {
              sql = sql + `AND confirmed = 0 `;
            }
          }
          sql = sql + `ORDER BY date DESC `;

          con.query(sql, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              let games:Game[] = [];

              result.forEach( (element) => {
                var userTimezoneOffset = element.date.getTimezoneOffset() * 60000;
                let newDate = new Date(element.date.getTime() - userTimezoneOffset);

                let game:Game = {
                  id: element.id,
                  competitionId: element.competition_id,
                  courtId: element.court_id,
                  localId: element.local_id,
                  visitantId: element.visitant_id,
                  date: newDate,
                  localScore: element.local_score,
                  visitantScore: element.visitant_score,
                  localFairPlay: element.local_fair_play,
                  visitantFairPlay: element.visitant_fair_play,
                  localMixed: !!element.local_mixed,
                  visitantMixed: !!element.visitant_mixed,
                  finished: !!element.finished,
                  confirmed: !!element.confirmed,
                };
                games.push(game)
              })

              return resolve(games);
          });
      });
  };

  updateGame = (game: Game): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE game SET
          competition_id = ?,
          court_id = ?,
          local_id = ?,
          visitant_id = ?,
          date = ?,
          local_score = ?,
          visitant_score = ?,
          local_fair_play = ?,
          visitant_fair_play = ?,
          local_mixed = ?,
          visitant_mixed = ?,
          finished = ?,
          confirmed = ? WHERE id = ?`;

          let values = [game.competitionId,
                        game.courtId,
                        game.localId,
                        game.visitantId,
                        game.date.toString(),
                        game.localScore,
                        game.visitantScore,
                        game.localFairPlay,
                        game.visitantFairPlay,
                        +!!game.localMixed,
                        +!!game.visitantMixed,
                        +!!game.finished,
                        +!!game.confirmed,
                        game.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  addGameResult = (game: Game): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE game SET
          local_score = ?,
          visitant_score = ?,
          visitant_fair_play = ?,
          visitant_mixed = ?,
          finished = ? WHERE id = ?`;

          let values = [game.localScore,
                        game.visitantScore,
                        game.visitantFairPlay,
                        +!!game.visitantMixed,
                        +!!game.finished,
                        game.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  addGameFeedback = (game: Game): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE game SET
          local_fair_play = ?,
          local_mixed = ?,
          confirmed = ? WHERE id = ?`;

          let values = [game.localFairPlay,
                        +!!game.localMixed,
                        +!!game.confirmed,
                        game.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteGame = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM game WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
