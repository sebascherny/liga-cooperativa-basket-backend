import MySqlConnection from './mysql.connection';

import CompetitionGroupRepository from '../../core/repositories/competitionGroup.repository';
import {CompetitionGroup, CompetitionGroupFilters} from '../../core/entities/CompetitionGroup';

export default class CompetitionGroupDatasource implements CompetitionGroupRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addCompetitionGroup = (competitionGroup: CompetitionGroup): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `INSERT INTO competition_group
          (league_id,
          name,
          start_date,
          end_date,
          active)
          VALUES (?,?,?,?,?)`;

          let values = [competitionGroup.leagueId,
                        competitionGroup.name,
                        competitionGroup.startDate,
                        competitionGroup.endDate,
                        +!!competitionGroup.active]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getCompetitionGroup = (id: number): Promise<CompetitionGroup> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM competition_group WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("CompetitionGroup with id="+id+" not found"));
              }

              let competitionGroup:CompetitionGroup = {
                id: result[0].id,
                leagueId: result[0].league_id,
                name: result[0].name,
                startDate: new Date(result[0].start_date),
                endDate: new Date(result[0].end_date),
                active: !!result[0].active
              };

              return resolve(competitionGroup);
          });
      });
  };

  listCompetitionGroups = (filters :CompetitionGroupFilters): Promise<CompetitionGroup[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM competition_group WHERE 1=1 ";

          if (filters.leagueId != null) {
            sql = sql + `AND league_id = ${filters.leagueId} `;
          }
          if (filters.active != null) {
            if (filters.active.toLowerCase() == 'true') {
              sql = sql + `AND active = 1 `;
            } else {
              sql = sql + `AND active = 0 `;
            }
          }

          con.query(sql, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              let competitionGroups:CompetitionGroup[] = [];
              result.forEach( (element) => {
                let competitionGroup:CompetitionGroup = {
                  id: element.id,
                  leagueId: element.league_id,
                  name: element.name,
                  startDate: new Date(element.start_date),
                  endDate: new Date(element.end_date),
                  active: !!element.active
                };
                competitionGroups.push(competitionGroup)
              })

              return resolve(competitionGroups);
          });
      });
  };

  updateCompetitionGroup = (competitionGroup: CompetitionGroup): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = `UPDATE competition_group SET
          league_id = ?,
          name = ?,
          start_date = ?,
          end_date = ?,
          active = ? WHERE id = ?`;

          let values = [competitionGroup.leagueId,
                        competitionGroup.name,
                        competitionGroup.startDate.toString(),
                        competitionGroup.endDate.toString(),
                        +!!competitionGroup.active,
                        competitionGroup.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteCompetitionGroup = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM competition_group WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
