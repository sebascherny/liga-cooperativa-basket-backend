const mysql = require('mysql2');

export default class MySqlConnection {

  public openConnection(){
    var con =  mysql.createConnection({
      host: process.env.HOST,
      user: process.env.USER_DB,
      password: process.env.PASSWORD,
      database: process.env.DATABASE
    });

    return con;
  }

  public async closeConnection(con){
    await con.end();
  }

}
