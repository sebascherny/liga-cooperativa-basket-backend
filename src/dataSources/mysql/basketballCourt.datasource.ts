import MySqlConnection from './mysql.connection';

import BasketballCourtRepository from '../../core/repositories/basketballCourt.repository';
import {BasketballCourt} from '../../core/entities/BasketballCourt';

export default class BasketballCourtDatasource implements BasketballCourtRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addBasketballCourt = (basketballCourt: BasketballCourt): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          // +!! changes boolean to 0 or 1
          let sql = `INSERT INTO basketball_court
            (name,
            direction,
            status,
            lights,
            light_timeout)
            VALUES (?,?,?,?,?)`;

          let values = [basketballCourt.name,
            basketballCourt.direction,
            basketballCourt.status,
            +!!basketballCourt.lights,
            basketballCourt.lightTimeout]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getBasketballCourt = (id: number): Promise<BasketballCourt> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM basketball_court WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("Basketball court with id="+id+" not found"));
              }

              // !! changes 0 or 1 to boolean
              let basketballCourt:BasketballCourt = {
                id: result[0].id,
                name: result[0].name,
                direction: result[0].direction,
                status: result[0].status,
                lights: !!result[0].lights,
                lightTimeout: result[0].light_timeout,
              };

              return resolve(basketballCourt);
          });
      });
  };

  listBasketballCourts = (): Promise<BasketballCourt[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM basketball_court";

          con.query(sql, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              let basketballCourts:BasketballCourt[] = [];
              result.forEach( (element) => {
                // !! changes 0 or 1 to boolean
                let basketballCourt:BasketballCourt = {
                  id: element.id,
                  name: element.name,
                  direction: element.direction,
                  status: element.status,
                  lights: !!element.lights,
                  lightTimeout: element.light_timeout,
                };
                basketballCourts.push(basketballCourt)
              })

              return resolve(basketballCourts);
          });
      });
  };

  updateBasketballCourt = (basketballCourt: BasketballCourt): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE basketball_court SET
          name = ?,
          direction = ?,
          status = ?,
          lights = ?,
          light_timeout = ? WHERE id =?`;

          let values = [basketballCourt.name,
            basketballCourt.direction,
            basketballCourt.status,
            +!!basketballCourt.lights,
            basketballCourt.lightTimeout,
            basketballCourt.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteBasketballCourt = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM basketball_court WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
