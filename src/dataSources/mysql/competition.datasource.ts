import MySqlConnection from './mysql.connection';

import CompetitionRepository from '../../core/repositories/competition.repository';
import {Competition, CompetitionFilters} from '../../core/entities/Competition';

export default class CompetitionDatasource implements CompetitionRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addCompetition = (competition: Competition): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `INSERT INTO competition
          (competition_group_id, name)
          VALUES (?,?)`;

          let values = [competition.competitionGroupId,
                        competition.name]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getCompetition = (id: number): Promise<Competition> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM competition WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("Competition with id="+id+" not found"));
              }

              let competition:Competition = {
                id: result[0].id,
                competitionGroupId: result[0].competition_group_id,
                name: result[0].name
              };

              return resolve(competition);
          });
      });
  };

  listCompetitions = (filters :CompetitionFilters): Promise<Competition[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM competition WHERE 1=1 ";

          if (filters.competitionGroupId != null) {
            sql = sql + `AND competition_group_id = ${filters.competitionGroupId} `;
          }

          con.query(sql, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              let competitions:Competition[] = [];
              result.forEach( (element) => {
                let competition:Competition = {
                  id: element.id,
                  competitionGroupId: element.competition_group_id,
                  name: element.name
                };
                competitions.push(competition)
              })

              return resolve(competitions);
          });
      });
  };

  updateCompetition = (competition: Competition): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE competition SET
          competition_group_id = ?,
          name = ? WHERE id = ?`;

          let values = [competition.competitionGroupId, competition.name, competition.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteCompetition = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM competition WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
