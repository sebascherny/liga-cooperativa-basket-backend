 import MySqlConnection from './mysql.connection';

import TeamRepository from '../../core/repositories/team.repository';
import {Team} from '../../core/entities/Team';

export default class TeamDatasource implements TeamRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

  addTeam = (team: Team): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `INSERT INTO team (
            league_id,
            name,
            photo) VALUES (?,?,?)`;

          let values = [team.leagueId,
                        team.name,
                        team.photo]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.insertId);
          });
      });
  };

  getTeam = (id: number): Promise<Team> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM team WHERE id=?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);

              if(error){
                  return reject(error);
              }

              if(result[0] == null) {
                return reject(Error("Team with id="+id+" not found"));
              }

              let team:Team = {
                id: result[0].id,
                leagueId: result[0].league_id,
                name: result[0].name,
                photo: result[0].photo
              };

              return resolve(team);
          });
      });
  };

  listTeams = (): Promise<Team[]> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "SELECT * FROM team";

          con.query(sql, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              let teams:Team[] = [];
              result.forEach( (element) => {
                let team:Team = {
                  id: element.id,
                  leagueId: element.league_id,
                  name: element.name,
                  photo: element.photo,
                };
                teams.push(team)
              })

              return resolve(teams);
          });
      });
  };

  updateTeam = (team: Team): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();

          let sql = `UPDATE team SET
          league_id = ?,
          name = ?,
          photo = ? WHERE id = ?`;

          let values = [team.leagueId,
                        team.name,
                        team.photo,
                        team.id]

          con.query(sql, values, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

  deleteTeam = (id: number): Promise<number> => {
      return new Promise((resolve, reject)=>{
          let con = this.mySqlConnection.openConnection();
          let sql = "DELETE FROM team WHERE id =?";

          con.query(sql, id, (error, result)=>{
              this.mySqlConnection.closeConnection(con);
              if(error){
                  return reject(error);
              }

              return resolve(result.affectedRows);
          });
      });
  };

}
