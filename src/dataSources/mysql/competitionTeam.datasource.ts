import MySqlConnection from './mysql.connection';

import CompetitionTeamRepository from '../../core/repositories/competitionTeam.repository';
import { CompetitionTeam } from '../../core/entities/CompetitionTeam';

export default class CompetitionTeamDatasource implements CompetitionTeamRepository {

  private mySqlConnection:MySqlConnection = new MySqlConnection();

    addCompetitionTeam = (competitionTeamItem: CompetitionTeam): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = `INSERT INTO competition_team (
              competition_id,
              team_id,
              victories,
              defeats,
              points_scored,
              points_received,
              competition_points,
              fair_play,
              mixed_games) VALUES
            (?,?,?,?,?,?,?,?,?)`;

            let values = [competitionTeamItem.competitionId,
                          competitionTeamItem.teamId,
                          competitionTeamItem.victories,
                          competitionTeamItem.defeats,
                          competitionTeamItem.pointsScored,
                          competitionTeamItem.pointsReceived,
                          competitionTeamItem.competitionPoints,
                          competitionTeamItem.fairPlay,
                          competitionTeamItem.mixedGames]

            con.query(sql, values, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.insertId);
            });
        });
    };

    getTeams = (competitionId: number): Promise<number[]> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT team_id FROM competition_team WHERE competition_id=?";

            con.query(sql, competitionId, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result);
            });
        });
    };

    getTeamsOrdered = (competitionId: number): Promise<number[]> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT team_id FROM competition_team WHERE competition_id=? ORDER BY competition_points DESC, ( points_scored - points_received ) DESC";

            con.query(sql, competitionId, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result);
            });
        });
    };

    getCompetitions = (teamId: number): Promise<number[]> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT competition_id FROM competition_team WHERE team_id=?";

            con.query(sql, teamId, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result);
            });
        });
    };

    getCompetitionTeam = (competitionId: number, teamId: number): Promise<CompetitionTeam> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT * FROM competition_team WHERE competition_id=? AND team_id=?";

            let values = [competitionId,teamId]

            con.query(sql, values, (error, result)=>{
                this.mySqlConnection.closeConnection(con);

                if(error){
                    return reject(error);
                }

                if(result[0] == null) {
                  return reject(Error("Competition Team pair not found"));
                }

                let competitionTeam:CompetitionTeam = {
                  id: result[0].id,
                  competitionId: result[0].competition_id,
                  teamId: result[0].team_id,
                  victories: result[0].victories,
                  defeats: result[0].defeats,
                  pointsScored: result[0].points_scored,
                  pointsReceived: result[0].points_received,
                  competitionPoints: result[0].competition_points,
                  fairPlay: result[0].fair_play,
                  mixedGames: result[0].mixed_games,
                };

                return resolve(competitionTeam);
            });
        });
    };

    listCompetitionTeams = (): Promise<CompetitionTeam[]> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "SELECT * FROM competition_team";

            con.query(sql, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                let competitionTeams:CompetitionTeam[] = [];
                result.forEach( (element) => {
                  let competitionTeam:CompetitionTeam = {
                    id: element.id,
                    competitionId: element.competition_id,
                    teamId: element.team_id,
                    victories: element.victories,
                    defeats: element.defeats,
                    pointsScored: element.points_scored,
                    pointsReceived: element.points_received,
                    competitionPoints: element.competition_points,
                    fairPlay: element.fair_play,
                    mixedGames: element.mixed_games,
                  };
                  competitionTeams.push(competitionTeam)
                })

                return resolve(competitionTeams);
            });
        });
    };

    updateCompetitionTeam = (competitionTeam: CompetitionTeam): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();

            let sql = `UPDATE competition_team SET
            competition_id = ?,
            team_id = ?,
            victories = ?,
            defeats = ?,
            points_scored = ?,
            points_received = ?,
            competition_points = ?,
            fair_play = ?,
            mixed_games = ? WHERE id = ?`;

            let values = [competitionTeam.competitionId,
                          competitionTeam.teamId,
                          competitionTeam.victories,
                          competitionTeam.defeats,
                          competitionTeam.pointsScored,
                          competitionTeam.pointsReceived,
                          competitionTeam.competitionPoints,
                          competitionTeam.fairPlay,
                          competitionTeam.mixedGames,
                          competitionTeam.id]

            con.query(sql, values, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.affectedRows);
            });
        });
    };

    deleteCompetitionTeam = (id: number): Promise<number> => {
        return new Promise((resolve, reject)=>{
            let con = this.mySqlConnection.openConnection();
            let sql = "DELETE FROM competition_team WHERE id =?";

            con.query(sql, id, (error, result)=>{
                this.mySqlConnection.closeConnection(con);
                if(error){
                    return reject(error);
                }

                return resolve(result.affectedRows);
            });
        });
    };

}
