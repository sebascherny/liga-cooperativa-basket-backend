import addGameController from '../controllers/Game/addGameHttp.controller';
import addGameResultController from '../controllers/Game/addGameResultHttp.controller';
import addGameFeedbackController from '../controllers/Game/addGameFeedbackHttp.controller';
import createGameController from '../controllers/Game/createGameHttp.controller';
import getGameController from '../controllers/Game/getGameHttp.controller';
import updateGameController from '../controllers/Game/updateGameHttp.controller';
import deleteGameController from '../controllers/Game/deleteGameHttp.controller';
import listGamesController from '../controllers/Game/listGamesHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const gameRouter = express.Router();

// API ENDPOINTS

// create game
      // competitionId: string;
      // courtId: string;
      // localId: string;
      // visitantId: string;
      // date: Date;
gameRouter.post('/create', auth, createGameController);

gameRouter.post('/', auth, addGameController);

gameRouter.get('/', listGamesController);

gameRouter.get('/:id', getGameController);

gameRouter.put('/:id', auth, updateGameController);

gameRouter.delete('/:id', auth, deleteGameController);

// add result (local team only)
// localScore: number;
// visitantScore: number;
// visitantFairPlay: number;
// visitantMixed: bool
gameRouter.put('/addResult/:id', auth, addGameResultController);

// add feedback (visitant team only -> completed = true)
// localFairPlay: number;
// localMixed: boolean;
gameRouter.put('/addFeedback/:id', auth, addGameFeedbackController);

module.exports = gameRouter;
