import addCompetitionController from '../controllers/Competition/addCompetitionHttp.controller';
import getCompetitionController from '../controllers/Competition/getCompetitionHttp.controller';
import listCompetitionsController from '../controllers/Competition/listCompetitionsHttp.controller';
import updateCompetitionController from '../controllers/Competition/updateCompetitionHttp.controller';
import deleteCompetitionController from '../controllers/Competition/deleteCompetitionHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const competitionRouter = express.Router();

// CRUD ENDPOINTS

competitionRouter.post('/', authAdmin, addCompetitionController);

competitionRouter.get('/:id', getCompetitionController);

competitionRouter.get('/', listCompetitionsController);

competitionRouter.put('/:id', authAdmin, updateCompetitionController);

competitionRouter.delete('/:id', authAdmin, deleteCompetitionController);

module.exports = competitionRouter;
