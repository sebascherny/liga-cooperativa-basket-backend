import addBasketballCourtController from '../controllers/BasketballCourt/addBasketballCourtHttp.controller';
import getBasketballCourtController from '../controllers/BasketballCourt/getBasketballCourtHttp.controller';
import updateBasketballCourtController from '../controllers/BasketballCourt/updateBasketballCourtHttp.controller';
import deleteBasketballCourtController from '../controllers/BasketballCourt/deleteBasketballCourtHttp.controller';
import listAvailableBasketballCourtsController from '../controllers/BasketballCourt/listAvailableBasketballCourtsHttp.controller';
import listBasketballCourtsController from '../controllers/BasketballCourt/listBasketballCourtsHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const basketballCourtRouter = express.Router();

// API ENDPOINTS
basketballCourtRouter.post('/', auth, addBasketballCourtController);

basketballCourtRouter.get('/available', listAvailableBasketballCourtsController);

basketballCourtRouter.get('/', listBasketballCourtsController);

basketballCourtRouter.get('/:id', getBasketballCourtController);

basketballCourtRouter.put('/:id', auth, updateBasketballCourtController);

basketballCourtRouter.delete('/:id', authAdmin, deleteBasketballCourtController);

module.exports = basketballCourtRouter;
