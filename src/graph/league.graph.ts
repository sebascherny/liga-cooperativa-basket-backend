import addLeagueController from '../controllers/League/addLeagueHttp.controller';
import getLeagueController from '../controllers/League/getLeagueHttp.controller';
import listLeaguesController from '../controllers/League/listLeaguesHttp.controller';
import updateLeagueController from '../controllers/League/updateLeagueHttp.controller';
import deleteLeagueController from '../controllers/League/deleteLeagueHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const leagueRouter = express.Router();

// API ENDPOINTS

leagueRouter.post('/', authAdmin, addLeagueController);

leagueRouter.get('/:id', getLeagueController);

leagueRouter.get('/', listLeaguesController);

leagueRouter.put('/:id', authAdmin, updateLeagueController);

leagueRouter.delete('/:id', authAdmin, deleteLeagueController);

module.exports = leagueRouter;
