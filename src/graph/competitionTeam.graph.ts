import addCompetitionTeamController from '../controllers/CompetitionTeam/addCompetitionTeamHttp.controller';
import getTeamsController from '../controllers/CompetitionTeam/getTeamsHttp.controller';
import getActiveCompetitionsController from '../controllers/CompetitionTeam/getActiveCompetitionsHttp.controller';
import getCompetitionTeamController from '../controllers/CompetitionTeam/getCompetitionTeamHttp.controller';
import listCompetitionClasificationsController from '../controllers/CompetitionTeam/listCompetitionClasificationsHttp.controller';
import updateCompetitionTeamController from '../controllers/CompetitionTeam/updateCompetitionTeamHttp.controller';
import deleteCompetitionTeamController from '../controllers/CompetitionTeam/deleteCompetitionTeamHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const competitionTeamRouter = express.Router();

// API ENDPOINTS

// list teams from same competition
competitionTeamRouter.get('/get-teams/:competitionId', getTeamsController);

competitionTeamRouter.get('/get-active-competitions/:teamId', auth, getActiveCompetitionsController);

competitionTeamRouter.post('/', authAdmin, addCompetitionTeamController);

competitionTeamRouter.delete('/:competitionId/:teamId', authAdmin, deleteCompetitionTeamController);

competitionTeamRouter.put('/', authAdmin, updateCompetitionTeamController);

competitionTeamRouter.get('/', auth, getCompetitionTeamController);

competitionTeamRouter.get('/clasifications', listCompetitionClasificationsController);

module.exports = competitionTeamRouter;
