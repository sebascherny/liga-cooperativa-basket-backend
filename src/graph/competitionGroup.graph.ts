import addCompetitionGroupController from '../controllers/CompetitionGroup/addCompetitionGroupHttp.controller';
import getCompetitionGroupController from '../controllers/CompetitionGroup/getCompetitionGroupHttp.controller';
import listCompetitionGroupsController from '../controllers/CompetitionGroup/listCompetitionGroupsHttp.controller';
import updateCompetitionGroupController from '../controllers/CompetitionGroup/updateCompetitionGroupHttp.controller';
import deleteCompetitionGroupController from '../controllers/CompetitionGroup/deleteCompetitionGroupHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const competitionGroupRouter = express.Router();

// API ENDPOINTS

competitionGroupRouter.post('/', authAdmin, addCompetitionGroupController);

competitionGroupRouter.get('/:id', getCompetitionGroupController);

competitionGroupRouter.get('/', listCompetitionGroupsController);

competitionGroupRouter.put('/:id', authAdmin, updateCompetitionGroupController);

competitionGroupRouter.delete('/:id', authAdmin, deleteCompetitionGroupController);

module.exports = competitionGroupRouter;
