import addUserController from '../controllers/User/addUserHttp.controller';
import getUserController from '../controllers/User/getUserHttp.controller';
import listUsersController from '../controllers/User/listUsersHttp.controller';
import updateUserController from '../controllers/User/updateUserHttp.controller';
import updatePasswordController from '../controllers/User/updatePasswordHttp.controller';
import deleteUserController from '../controllers/User/deleteUserHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const userRouter = express.Router();

// API ENDPOINTS
userRouter.post('/', authAdmin, addUserController);

userRouter.get('/:id', authAdmin, getUserController);

userRouter.get('/', authAdmin, listUsersController);

userRouter.put('/:id', authAdmin, updateUserController);

userRouter.delete('/:id', authAdmin, deleteUserController);

userRouter.put('/:id/update-password', auth, updatePasswordController);


module.exports = userRouter;
