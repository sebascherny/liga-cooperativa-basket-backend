import addTeamController from '../controllers/Team/addTeamHttp.controller';
import getTeamController from '../controllers/Team/getTeamHttp.controller';
import updateTeamController from '../controllers/Team/updateTeamHttp.controller';
import deleteTeamController from '../controllers/Team/deleteTeamHttp.controller';
import listTeamsController from '../controllers/Team/listTeamsHttp.controller';
import listTeamsPendingGamesController from '../controllers/Team/listTeamsPendingGamesHttp.controller';

import {auth, authAdmin} from '../controllers/Authentication/auth.controller';

const express = require('express');
const teamRouter = express.Router();

// API ENDPOINTS

teamRouter.post('/', authAdmin, addTeamController);

teamRouter.get('/', listTeamsController);

teamRouter.get('/:id', getTeamController);

teamRouter.get('/:teamId/pending-games', auth, listTeamsPendingGamesController);

teamRouter.put('/:id', authAdmin, updateTeamController);

teamRouter.delete('/:id', authAdmin, deleteTeamController);

module.exports = teamRouter;
