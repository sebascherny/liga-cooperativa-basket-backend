import loginController from '../controllers/User/loginHttp.controller';

const express = require('express');
const loginRouter = express.Router();

// API ENDPOINTS
loginRouter.post('/', loginController);

module.exports = loginRouter;
