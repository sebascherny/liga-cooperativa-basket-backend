require('dotenv').config();
import express from 'express';
const cors = require('cors');
const bodyParser = require('body-parser');

const basketballCourtRouter = require('./graph/basketballCourt.graph');
const competitionRouter = require('./graph/competition.graph');
const competitionTeamRouter = require('./graph/competitionTeam.graph');
const competitionGroupRouter = require('./graph/competitionGroup.graph');
const gameRouter = require('./graph/game.graph');
const leagueRouter = require('./graph/league.graph');
const teamRouter = require('./graph/team.graph');
const userRouter = require('./graph/user.graph');
const loginRouter = require('./graph/login.graph');

const PORT = 8080;
const app = express();
app.use(express.json({limit: '25mb'}));
app.use(bodyParser.json({limit: '25mb'}));
app.use(cors());

// API ENDPOINTS
app.use('/basketball-court', basketballCourtRouter);
app.use('/competition', competitionRouter);
app.use('/competition-team', competitionTeamRouter);
app.use('/competition-group', competitionGroupRouter);
app.use('/game', gameRouter);
app.use('/league', leagueRouter);
app.use('/team', teamRouter);
app.use('/user', userRouter);
app.use('/login', loginRouter);

app.listen(PORT, () => {
  console.log(`server started at http://localhost:${PORT}`);
});
