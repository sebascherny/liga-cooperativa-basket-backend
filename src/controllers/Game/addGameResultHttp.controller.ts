import { Request, Response } from 'express';
import addGameResult from '../../core/interactors/Game/addGameResult.interactor';
import {AddGameResultInput} from '../../core/entities/Game';

const addGameResultController = async (request: Request, response: Response) => {
  const { body } = request;

  let addGameResultItem = <AddGameResultInput>{};
  addGameResultItem.id = Number(request.params.id);
  addGameResultItem.localScore = body.localScore;
  addGameResultItem.visitantScore = body.visitantScore;
  addGameResultItem.visitantFairPlay = body.visitantFairPlay;
  addGameResultItem.visitantMixed = body.visitantMixed;
  if (body.visitantMixed == 'false') {
    addGameResultItem.visitantMixed = false;
  }
  if (body.visitantMixed == 'true') {
    addGameResultItem.visitantMixed = true;
  }

  try {
    const gameItemResult = await addGameResult(addGameResultItem);
    response.json(gameItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addGameResultController;
