import { Request, Response } from 'express';
import listGames from '../../core/interactors/Game/listGames.interactor';
import listGamesExpanded from '../../core/interactors/Game/listGamesExpanded.interactor';
import type { GameFilters } from '../../core/entities/Game';

const listGamesController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    let gameFilters = <GameFilters>{};
    if (filters.competitionId != null){
      gameFilters.competitionId = filters.competitionId.toString();
    }
    if (filters.courtId != null){
      gameFilters.courtId = filters.courtId.toString();
    }
    if (filters.teamId != null){
      gameFilters.teamId = filters.teamId.toString();
    }
    if (filters.localId != null){
      gameFilters.localId = filters.localId.toString();
    }
    if (filters.visitantId != null){
      gameFilters.visitantId = filters.visitantId.toString();
    }
    if (filters.startDate != null){
      gameFilters.startDate = filters.startDate.toString();
    }
    if (filters.endDate != null){
      gameFilters.endDate = filters.endDate.toString();
    }
    if (filters.finished != null){
      gameFilters.finished = filters.finished.toString();
    }
    if (filters.confirmed != null){
      gameFilters.confirmed = filters.confirmed.toString();
    }

    if (filters.expanded != null){
      if (filters.expanded.toString() == 'true') {
        const games = await listGamesExpanded(gameFilters);
        response.json(games);
        return;
      }
    }

    const games = await listGames(gameFilters);
    response.json(games);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listGamesController;
