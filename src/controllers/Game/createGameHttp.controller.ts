import { Request, Response } from 'express';
import createGame from '../../core/interactors/Game/createGame.interactor';
import {CreateGameInput} from '../../core/entities/Game';

const createGameController = async (request: Request, response: Response) => {
  const { body } = request;
  const { competitionId } = body;
  const { courtId } = body;
  const { localId } = body;
  const { visitantId } = body;
  const { date } = body;

  let createGameItem = <CreateGameInput>{};
  createGameItem.competitionId = body.competitionId;
  createGameItem.courtId = body.courtId;
  createGameItem.localId = body.localId;
  createGameItem.visitantId = body.visitantId;
  createGameItem.date = body.date;

  try {
    const gameItemResult = await createGame(createGameItem);
    response.json(gameItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default createGameController;
