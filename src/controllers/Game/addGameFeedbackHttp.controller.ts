import { Request, Response } from 'express';
import addGameFeedback from '../../core/interactors/Game/addGameFeedback.interactor';
import {AddGameFeedbackInput} from '../../core/entities/Game';

const addGameFeedbackController = async (request: Request, response: Response) => {
  const { body } = request;

  let addGameFeedbackItem = <AddGameFeedbackInput>{};
  addGameFeedbackItem.id = Number(request.params.id);
  addGameFeedbackItem.localFairPlay = body.localFairPlay;
  addGameFeedbackItem.localMixed = body.localMixed;

  try {
    const gameItemResult = await addGameFeedback(addGameFeedbackItem);
    response.json(gameItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addGameFeedbackController;
