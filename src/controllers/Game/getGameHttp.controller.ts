import { Request, Response } from 'express';
import getGame from '../../core/interactors/Game/getGame.interactor';

const getGameController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const game = await getGame(Number(id));
    response.json(game);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getGameController;
