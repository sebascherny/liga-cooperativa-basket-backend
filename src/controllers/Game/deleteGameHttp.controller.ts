import { Request, Response } from 'express';
import deleteGame from '../../core/interactors/Game/deleteGame.interactor';

const deleteGameController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const gameItem = await deleteGame(Number(id));
    response.json(gameItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteGameController;
