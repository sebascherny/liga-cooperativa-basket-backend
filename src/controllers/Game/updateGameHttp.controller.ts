import { Request, Response } from 'express';
import updateGame from '../../core/interactors/Game/updateGame.interactor';
import {UpdateGameInput} from '../../core/entities/Game';

const updateGameController = async (request: Request, response: Response) => {
  const { body } = request;

  let updateGameItem = <UpdateGameInput>{};
  updateGameItem.id = Number(request.params.id);
  updateGameItem.competitionId = body.competitionId;
  updateGameItem.courtId = body.courtId;
  updateGameItem.localId = body.localId;
  updateGameItem.visitantId = body.visitantId;
  updateGameItem.date = body.date;
  updateGameItem.localScore = body.localScore;
  updateGameItem.visitantScore = body.visitantScore;
  updateGameItem.localFairPlay = body.localFairPlay;
  updateGameItem.visitantFairPlay = body.visitantFairPlay;
  updateGameItem.localMixed = body.localMixed;
  updateGameItem.visitantMixed = body.visitantMixed;
  if (body.visitantMixed == 'false') {
    updateGameItem.visitantMixed = false;
  }
  if (body.visitantMixed == 'true') {
    updateGameItem.visitantMixed = true;
  }
  updateGameItem.finished = body.finished;
  updateGameItem.confirmed = body.confirmed;

  try {
    const gameItemResult = await updateGame(updateGameItem);
    response.json(gameItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateGameController;
