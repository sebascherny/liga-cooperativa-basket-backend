import { Request, Response } from 'express';
import addGame from '../../core/interactors/Game/addGame.interactor';
import {AddGameInput} from '../../core/entities/Game';

const addGameController = async (request: Request, response: Response) => {
  const { body } = request;

  let addGameItem = <AddGameInput>{};
  addGameItem.competitionId = body.competitionId;
  addGameItem.courtId = body.courtId;
  addGameItem.localId = body.localId;
  addGameItem.visitantId = body.visitantId;
  addGameItem.date = body.date;
  addGameItem.localScore = body.localScore;
  addGameItem.visitantScore = body.visitantScore;
  addGameItem.localFairPlay = body.localFairPlay;
  addGameItem.visitantFairPlay = body.visitantFairPlay;
  addGameItem.localMixed = body.localMixed;
  addGameItem.visitantMixed = body.visitantMixed;
  if (body.visitantMixed == 'false') {
    addGameItem.visitantMixed = false;
  }
  if (body.visitantMixed == 'true') {
    addGameItem.visitantMixed = true;
  }
  addGameItem.finished = body.finished;
  addGameItem.confirmed = body.confirmed;

  try {
    const gameItemResult = await addGame(addGameItem);
    response.json(gameItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addGameController;
