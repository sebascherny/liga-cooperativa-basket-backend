import { Request, Response } from 'express';
import updateCompetitionGroup from '../../core/interactors/CompetitionGroup/updateCompetitionGroup.interactor';
import {UpdateCompetitionGroupInput} from '../../core/entities/CompetitionGroup';

const updateCompetitionGroupController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionGroupItem = <UpdateCompetitionGroupInput>{};
  competitionGroupItem.id = Number(request.params.id);
  competitionGroupItem.leagueId = body.leagueId;
  competitionGroupItem.name = body.name;
  competitionGroupItem.startDate = body.startDate;
  competitionGroupItem.endDate = body.endDate;
  competitionGroupItem.active = body.active;

  try {
    const competitionGroupItemResult = await updateCompetitionGroup(competitionGroupItem);
    response.json(competitionGroupItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateCompetitionGroupController;
