import { Request, Response } from 'express';
import listCompetitionGroups from '../../core/interactors/CompetitionGroup/listCompetitionGroups.interactor';
import type { CompetitionGroupFilters } from '../../core/entities/CompetitionGroup';

const listCompetitionGroupsController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    let competitionClasificationGroupFilters = <CompetitionGroupFilters>{};
    if (filters.leagueId != null){
      competitionClasificationGroupFilters.leagueId = filters.leagueId.toString();
    }
    if (filters.active != null){
      competitionClasificationGroupFilters.active = filters.active.toString();
    }
    const competitionGroups = await listCompetitionGroups(competitionClasificationGroupFilters);
    response.json(competitionGroups);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listCompetitionGroupsController;
