import { Request, Response } from 'express';
import deleteCompetitionGroup from '../../core/interactors/CompetitionGroup/deleteCompetitionGroup.interactor';

const deleteCompetitionGroupController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const competitionGroupItem = await deleteCompetitionGroup(Number(id));
    response.json(competitionGroupItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteCompetitionGroupController;
