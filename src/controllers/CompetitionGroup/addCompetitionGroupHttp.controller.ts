import { Request, Response } from 'express';
import addCompetitionGroup from '../../core/interactors/CompetitionGroup/addCompetitionGroup.interactor';
import {AddCompetitionGroupInput} from '../../core/entities/CompetitionGroup';

const addCompetitionGroupController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionGroupItem = <AddCompetitionGroupInput>{};
  competitionGroupItem.leagueId = body.leagueId;
  competitionGroupItem.name = body.name;
  competitionGroupItem.startDate = body.startDate;
  competitionGroupItem.endDate = body.endDate;
  competitionGroupItem.active = body.active;

  try {
    const competitionGroupItemResult = await addCompetitionGroup(competitionGroupItem);
    response.json(competitionGroupItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addCompetitionGroupController;
