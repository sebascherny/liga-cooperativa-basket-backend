import { Request, Response } from 'express';
import getCompetitionGroup from '../../core/interactors/CompetitionGroup/getCompetitionGroup.interactor';

const getCompetitionGroupController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const competitionGroup = await getCompetitionGroup(Number(id));
    response.json(competitionGroup);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getCompetitionGroupController;
