import { Request, Response } from 'express';
import getCompetition from '../../core/interactors/Competition/getCompetition.interactor';

const getCompetitionController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const competition = await getCompetition(Number(id));
    response.json(competition);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getCompetitionController;
