import { Request, Response } from 'express';
import addCompetition from '../../core/interactors/Competition/addCompetition.interactor';
import {AddCompetitionInput} from '../../core/entities/Competition';

const addCompetitionController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionItem = <AddCompetitionInput>{};
  competitionItem.competitionGroupId = body.competitionGroupId;
  competitionItem.name = body.name;

  try {
    const competitionItemResult = await addCompetition(competitionItem);
    response.json(competitionItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addCompetitionController;
