import { Request, Response } from 'express';
import updateCompetition from '../../core/interactors/Competition/updateCompetition.interactor';
import {UpdateCompetitionInput} from '../../core/entities/Competition';

const updateCompetitionController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionItem = <UpdateCompetitionInput>{};
  competitionItem.id = Number(request.params.id);
  competitionItem.competitionGroupId = body.competitionGroupId;
  competitionItem.name = body.name;

  try {
    const competitionItemResult = await updateCompetition(competitionItem);
    response.json(competitionItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateCompetitionController;
