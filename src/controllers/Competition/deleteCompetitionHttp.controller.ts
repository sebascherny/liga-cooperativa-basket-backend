import { Request, Response } from 'express';
import deleteCompetition from '../../core/interactors/Competition/deleteCompetition.interactor';

const deleteCompetitionController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const competitionItem = await deleteCompetition(Number(id));
    response.json(competitionItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteCompetitionController;
