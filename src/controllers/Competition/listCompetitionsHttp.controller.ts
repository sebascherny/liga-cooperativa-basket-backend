import { Request, Response } from 'express';
import listCompetitions from '../../core/interactors/Competition/listCompetitions.interactor';
import type { CompetitionFilters } from '../../core/entities/Competition';

const listCompetitionsController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    let competitionClasificationFilters = <CompetitionFilters>{};
    if (filters.competitionGroupId != null){
      competitionClasificationFilters.competitionGroupId = filters.competitionId.toString();
    }

    const competitions = await listCompetitions(competitionClasificationFilters);
    response.json(competitions);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listCompetitionsController;
