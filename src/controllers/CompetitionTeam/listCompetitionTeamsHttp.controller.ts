import { Request, Response } from 'express';
import listCompetitionTeams from '../../core/interactors/CompetitionTeam/listCompetitionTeams.interactor';

const listCompetitionTeamsController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    const competitionTeams = await listCompetitionTeams();
    response.json(competitionTeams);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listCompetitionTeamsController;
