import { Request, Response } from 'express';
import getActiveCompetitions from '../../core/interactors/CompetitionTeam/getActiveCompetitions.interactor';

const getActiveCompetitionsController = async (request: Request, response: Response) => {

  const { params } = request;
  const { teamId } = params;

  try {
    const competitions = await getActiveCompetitions(Number(teamId));
    response.json(competitions);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getActiveCompetitionsController;
