import { Request, Response } from 'express';
import listCompetitionClasifications from '../../core/interactors/CompetitionTeam/listCompetitionClasifications.interactor';
import type { CompetitionFilters } from '../../core/entities/Competition';

const listCompetitionClasificationsController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    let competitionClasificationFilters = <CompetitionFilters>{};
    if (filters.competitionGroupId != null){
      competitionClasificationFilters.competitionGroupId = filters.competitionGroupId.toString();
    }

    const competitionClasifications = await listCompetitionClasifications(competitionClasificationFilters);
    response.json(competitionClasifications);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listCompetitionClasificationsController;
