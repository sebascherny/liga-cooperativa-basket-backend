import { Request, Response } from 'express';
import getTeams from '../../core/interactors/CompetitionTeam/getTeams.interactor';
import getTeamsExpanded from '../../core/interactors/CompetitionTeam/getTeamsExpanded.interactor';

const getTeamsController = async (request: Request, response: Response) => {

  const { params } = request;
  const { competitionId } = params;
  let filters = request.query;

  try {

    if (filters.expanded != null){
      if (filters.expanded.toString() == 'true') {
        const teams = await getTeamsExpanded(Number(competitionId));
        response.json(teams);
        return;
      }
    }

    const teams = await getTeams(Number(competitionId));
    response.json(teams);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getTeamsController;
