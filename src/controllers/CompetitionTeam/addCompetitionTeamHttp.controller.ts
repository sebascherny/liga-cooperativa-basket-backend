import { Request, Response } from 'express';
import addCompetitionTeam from '../../core/interactors/CompetitionTeam/addCompetitionTeam.interactor';
import {AddCompetitionTeamInput} from '../../core/entities/CompetitionTeam';

const addCompetitionTeamController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionTeamItem = <AddCompetitionTeamInput>{};
  competitionTeamItem.competitionId = body.competitionId;
  competitionTeamItem.teamId = body.teamId;
  competitionTeamItem.victories = body.victories;
  competitionTeamItem.defeats = body.defeats;
  competitionTeamItem.pointsScored = body.pointsScored;
  competitionTeamItem.pointsReceived = body.pointsReceived;
  competitionTeamItem.competitionPoints = body.competitionPoints;
  competitionTeamItem.fairPlay = body.fairPlay;
  competitionTeamItem.mixedGames = body.mixedGames;

  try {
    const competitionTeamItemResult = await addCompetitionTeam(competitionTeamItem);
    response.json(competitionTeamItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addCompetitionTeamController;
