import { Request, Response } from 'express';
import updateCompetitionTeam from '../../core/interactors/CompetitionTeam/updateCompetitionTeam.interactor';
import {UpdateCompetitionTeamInput} from '../../core/entities/CompetitionTeam';

const updateCompetitionTeamController = async (request: Request, response: Response) => {
  const { body } = request;

  let competitionTeamItem = <UpdateCompetitionTeamInput>{};
  competitionTeamItem.competitionId = Number(body.competitionId);
  competitionTeamItem.teamId = Number(body.teamId);
  competitionTeamItem.victories = body.victories;
  competitionTeamItem.defeats = body.defeats;
  competitionTeamItem.pointsScored = body.pointsScored;
  competitionTeamItem.pointsReceived = body.pointsReceived;
  competitionTeamItem.competitionPoints = body.competitionPoints;
  competitionTeamItem.fairPlay = body.fairPlay;
  competitionTeamItem.mixedGames = body.mixedGames;

  try {
    const competitionTeamItemResult = await updateCompetitionTeam(competitionTeamItem);
    response.json(competitionTeamItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateCompetitionTeamController;
