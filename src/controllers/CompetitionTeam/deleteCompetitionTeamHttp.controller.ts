import { Request, Response } from 'express';
import deleteCompetitionTeam from '../../core/interactors/CompetitionTeam/deleteCompetitionTeam.interactor';

const deleteCompetitionTeamController = async (request: Request, response: Response) => {
  const { params } = request;
  const { competitionId } = params;
  const { teamId } = params;

  try {
    const competitionTeamItem = await deleteCompetitionTeam(Number(competitionId), Number(teamId));
    response.json(competitionTeamItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteCompetitionTeamController;
