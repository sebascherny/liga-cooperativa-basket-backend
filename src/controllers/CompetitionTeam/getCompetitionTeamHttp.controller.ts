import { Request, Response } from 'express';
import getCompetitionTeam from '../../core/interactors/CompetitionTeam/getCompetitionTeam.interactor';
import getCompetitionTeamExpanded from '../../core/interactors/CompetitionTeam/getCompetitionTeamExpanded.interactor';

const getCompetitionTeamController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {

    if (filters.expanded != null){
      if (filters.expanded.toString() == 'true') {
        const competitionTeam = await getCompetitionTeamExpanded(Number(filters.competitionId),Number(filters.teamId));
        response.json(competitionTeam);
        return;
      }
    }

    const competitionTeam = await getCompetitionTeam(Number(filters.competitionId),Number(filters.teamId));
    response.json(competitionTeam);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getCompetitionTeamController;
