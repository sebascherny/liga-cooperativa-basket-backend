import { Request, Response } from 'express';
import getTeam from '../../core/interactors/Team/getTeam.interactor';

const getTeamController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const team = await getTeam(Number(id));
    response.json(team);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getTeamController;
