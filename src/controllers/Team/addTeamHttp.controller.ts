import { Request, Response } from 'express';
import addTeam from '../../core/interactors/Team/addTeam.interactor';
import {AddTeamInput} from '../../core/entities/Team';

const addTeamController = async (request: Request, response: Response) => {
  const { body } = request;

  let teamItem = <AddTeamInput>{};
  teamItem.leagueId = body.leagueId;
  teamItem.name = body.name;
  teamItem.photo = body.photo;

  try {
    const teamItemResult = await addTeam(teamItem);
    response.json(teamItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addTeamController;
