import { Request, Response } from 'express';
import deleteTeam from '../../core/interactors/Team/deleteTeam.interactor';

const deleteTeamController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const teamItem = await deleteTeam(Number(id));
    response.json(teamItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteTeamController;
