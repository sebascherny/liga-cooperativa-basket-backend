import { Request, Response } from 'express';
import listTeams from '../../core/interactors/Team/listTeams.interactor';

const listTeamsController = async (request: Request, response: Response) => {

  const { params } = request;

  try {
    const teams = await listTeams();
    response.json(teams);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listTeamsController;
