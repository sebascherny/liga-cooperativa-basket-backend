import { Request, Response } from 'express';
import listTeamsPendingGames from '../../core/interactors/Team/listTeamsPendingGames.interactor';

const listTeamsPendingGamesController = async (request: Request, response: Response) => {

  const { params } = request;
  const { teamId } = params;
  let filters = request.query;

  try {
    if (filters.competitionId == null){
      return Error("CompetitionId is needed");
    }
    const teams = await listTeamsPendingGames(filters.competitionId.toString(),teamId.toString());
    response.json(teams);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listTeamsPendingGamesController;
