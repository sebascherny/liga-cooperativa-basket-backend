import { Request, Response } from 'express';
import updateTeam from '../../core/interactors/Team/updateTeam.interactor';
import {UpdateTeamInput} from '../../core/entities/Team';

const updateTeamController = async (request: Request, response: Response) => {
  const { body } = request;

  let teamItem = <UpdateTeamInput>{};
  teamItem.id = Number(request.params.id);
  teamItem.leagueId = body.leagueId;
  teamItem.name = body.name;
  teamItem.photo = body.photo;


  try {
    const teamItemResult = await updateTeam(teamItem);
    response.json(teamItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateTeamController;
