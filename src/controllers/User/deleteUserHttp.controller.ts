import { Request, Response } from 'express';
import deleteUser from '../../core/interactors/User/deleteUser.interactor';

const deleteUserController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const userItem = await deleteUser(Number(id));
    response.json(userItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteUserController;
