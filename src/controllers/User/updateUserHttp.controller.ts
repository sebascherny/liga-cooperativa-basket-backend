import { Request, Response } from 'express';
import updateUser from '../../core/interactors/User/updateUser.interactor';
import {UpdateUserInput} from '../../core/entities/User';

const updateUserController = async (request: Request, response: Response) => {
  const { body } = request;

  let userItem = <UpdateUserInput>{};
  userItem.id = Number(request.params.id);
  userItem.teamId = body.teamId;
  userItem.name = body.name;
  userItem.email = body.email;
  userItem.password = body.password;
  userItem.admin = body.admin;

  try {
    const userItemResult = await updateUser(userItem);

    response.json(userItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateUserController;
