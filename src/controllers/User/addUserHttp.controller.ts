import { Request, Response } from 'express';
import addUser from '../../core/interactors/User/addUser.interactor';
import {AddUserInput} from '../../core/entities/User';

const addUserController = async (request: Request, response: Response) => {
  const { body } = request;
  const { teamId } = body;
  const { name } = body;
  const { password } = body;
  const { email } = body;
  const { admin } = body;

  let userItem = <AddUserInput>{};
  userItem.teamId = body.teamId;
  userItem.name = body.name;
  userItem.email = body.email;
  userItem.password = body.password;
  userItem.admin = body.admin;

  try {
    const userItemResult = await addUser(userItem);
    response.json(userItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addUserController;
