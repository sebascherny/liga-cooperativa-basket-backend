import { Request, Response } from 'express';
import login from '../../core/interactors/User/login.interactor';
import { UserFilters } from '../../core/entities/User';

const loginController = async (request: Request, response: Response) => {
  const userAndPasswordBase64 = request.header('Authorization')?.replace('Basic ', '');

  try {

    let buff = Buffer.from(userAndPasswordBase64, 'base64');
    const name = buff.toString('ascii').split(":")[0];
    const password = buff.toString('ascii').split(":")[1];

    let userFilters = <UserFilters>{};
    if (name != null) {
      userFilters.name = name;
    }
    if (password != null) {
      userFilters.password = password;
    }

    let result = await login(userFilters);

    response.json(result);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default loginController;
