import { Request, Response } from 'express';
import getUser from '../../core/interactors/User/getUser.interactor';

const getUserController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const user = await getUser(Number(id));
    response.json(user);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getUserController;
