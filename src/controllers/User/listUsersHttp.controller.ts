import { Request, Response } from 'express';
import listUsers from '../../core/interactors/User/listUsers.interactor';
import { UserFilters } from '../../core/entities/User';

const listUsersController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    let userFilters = <UserFilters>{};
    if (filters.teamId != null){
      userFilters.teamId = filters.teamId.toString();
    }
    if (filters.name != null){
      userFilters.name = filters.name.toString();
    }
    if (filters.email != null){
      userFilters.email = filters.email.toString();
    }
    const users = await listUsers(userFilters);

    response.json(users);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listUsersController;
