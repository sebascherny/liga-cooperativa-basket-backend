import { Request, Response } from 'express';
import updatePassword from '../../core/interactors/User/updatePassword.interactor';
import {UpdatePasswordInput} from '../../core/entities/User';

const updatePasswordController = async (request: Request, response: Response) => {
  const { body } = request;

  let passwordItem = <UpdatePasswordInput>{};
  passwordItem.id = Number(request.params.id);
  passwordItem.oldPassword = body.oldPassword;
  passwordItem.newPassword = body.newPassword;


  try {
    const passwordItemResult = await updatePassword(passwordItem);

    response.json(passwordItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updatePasswordController;
