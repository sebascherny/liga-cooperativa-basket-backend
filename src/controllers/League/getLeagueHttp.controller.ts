import { Request, Response } from 'express';
import getLeague from '../../core/interactors/League/getLeague.interactor';

const getLeagueController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
  const league = await getLeague(Number(id));
  response.json(league);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getLeagueController;
