import { Request, Response } from 'express';
import deleteLeague from '../../core/interactors/League/deleteLeague.interactor';

const deleteLeagueController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const leagueItem = await deleteLeague(Number(id));
    response.json(leagueItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteLeagueController;
