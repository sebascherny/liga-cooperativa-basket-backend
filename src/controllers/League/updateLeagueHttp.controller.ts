import { Request, Response } from 'express';
import updateLeague from '../../core/interactors/League/updateLeague.interactor';
import {UpdateLeagueInput} from '../../core/entities/League';

const updateLeagueController = async (request: Request, response: Response) => {
  const { body } = request;

  let leagueItem = <UpdateLeagueInput>{};
  leagueItem.id = Number(request.params.id);
  leagueItem.name = body.name;
  leagueItem.year = body.year;

  try {
    const leagueItemResult = await updateLeague(leagueItem);
    response.json(leagueItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateLeagueController;
