import { Request, Response } from 'express';
import listLeagues from '../../core/interactors/League/listLeagues.interactor';

const listLeaguesController = async (request: Request, response: Response) => {

  const { params } = request;

  try {
  const leagues = await listLeagues();
  response.json(leagues);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listLeaguesController;
