import { Request, Response } from 'express';
import addLeague from '../../core/interactors/League/addLeague.interactor';
import {AddLeagueInput} from '../../core/entities/League';

const addLeagueController = async (request: Request, response: Response) => {
  const { body } = request;

  let leagueItem = <AddLeagueInput>{};
  leagueItem.name = body.name;
  leagueItem.year = body.year;

  try {
    const leagueItemResult = await addLeague(leagueItem);
    response.json(leagueItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addLeagueController;
