import { Request, Response } from 'express';
import addBasketballCourt from '../../core/interactors/BasketballCourt/addBasketballCourt.interactor';
import {AddBasketballCourtInput} from '../../core/entities/BasketballCourt';

const addBasketballCourtController = async (request: Request, response: Response) => {
  const { body } = request;

  let basketballCourtItem = <AddBasketballCourtInput>{};
  basketballCourtItem.name = body.name;
  basketballCourtItem.direction = body.direction;
  basketballCourtItem.status = body.status;
  basketballCourtItem.lights = body.lights;
  basketballCourtItem.lightTimeout = body.slightTimeout;

  try {
    const basketballCourtItemResult = await addBasketballCourt(basketballCourtItem);
    response.json(basketballCourtItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default addBasketballCourtController;
