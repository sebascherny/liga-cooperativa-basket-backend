import { Request, Response } from 'express';
import listAvailableBasketballCourts from '../../core/interactors/BasketballCourt/listAvailableBasketballCourts.interactor';

const listAvailableBasketballCourtsController = async (request: Request, response: Response) => {

  const { params } = request;
  let filters = request.query;

  try {
    if (filters.date == null){
      throw new Error('Hace falta la fecha para listar las canchas disponibles');
    }
    let date = filters.date.toString();
    
    const basketballCourts = await listAvailableBasketballCourts(date);
    response.json(basketballCourts);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listAvailableBasketballCourtsController;
