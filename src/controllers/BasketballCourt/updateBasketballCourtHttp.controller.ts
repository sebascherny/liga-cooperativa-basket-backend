import { Request, Response } from 'express';
import updateBasketballCourt from '../../core/interactors/BasketballCourt/updateBasketballCourt.interactor';
import {UpdateBasketballCourtInput} from '../../core/entities/BasketballCourt';

const updateBasketballCourtController = async (request: Request, response: Response) => {
  const { body } = request;

  let basketballCourtItem = <UpdateBasketballCourtInput>{};
  basketballCourtItem.id = Number(request.params.id);
  basketballCourtItem.name = body.name;
  basketballCourtItem.direction = body.direction;
  basketballCourtItem.status = body.status;
  basketballCourtItem.lights = body.lights;
  basketballCourtItem.lightTimeout = body.slightTimeout;

  try {
    const basketballCourtItemResult = await updateBasketballCourt(basketballCourtItem);
    response.json(basketballCourtItemResult);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default updateBasketballCourtController;
