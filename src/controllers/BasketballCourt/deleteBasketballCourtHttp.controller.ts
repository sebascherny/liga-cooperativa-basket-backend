import { Request, Response } from 'express';
import deleteBasketballCourt from '../../core/interactors/BasketballCourt/deleteBasketballCourt.interactor';

const deleteBasketballCourtController = async (request: Request, response: Response) => {
  const id = request.params.id;

  try {
    const basketballCourtItem = await deleteBasketballCourt(Number(id));
    response.json(basketballCourtItem);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default deleteBasketballCourtController;
