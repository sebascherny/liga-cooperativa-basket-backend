import { Request, Response } from 'express';
import getBasketballCourt from '../../core/interactors/BasketballCourt/getBasketballCourt.interactor';

const getBasketballCourtController = async (request: Request, response: Response) => {

  const { params } = request;
  const { id } = params;

  try {
    const basketballCourt = await getBasketballCourt(Number(id));
    response.json(basketballCourt);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default getBasketballCourtController;
