import { Request, Response } from 'express';
import listBasketballCourts from '../../core/interactors/BasketballCourt/listBasketballCourts.interactor';

const listBasketballCourtsController = async (request: Request, response: Response) => {

  const { params } = request;

  try {
    const basketballCourts = await listBasketballCourts();
    response.json(basketballCourts);
  } catch (error) {
    response.status(400).send(error.message);
  }
}

export default listBasketballCourtsController;
