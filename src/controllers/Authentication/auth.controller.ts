import jwt, { JwtPayload } from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import getUser from '../../core/interactors/User/getUser.interactor';

export interface CustomRequest extends Request {
 token: string | JwtPayload;
}

export const auth = async (req: Request, res: Response, next: NextFunction) => {
 try {
   const token = req.header('Authorization')?.replace('Bearer ', '');
   if (!token) {
     throw new Error();
   }

   const decoded = jwt.verify(token, process.env.SECRET_KEY);
   (req as CustomRequest).token = decoded;

   next();
 } catch (err) {
   res.status(401).send('Please authenticate');
 }
};

export const authAdmin = async (req: Request, res: Response, next: NextFunction) => {
 try {
   const token = req.header('Authorization')?.replace('Bearer ', '');
   if (!token) {
     throw new Error();
   }

   const decoded = await jwt.verify(token, process.env.SECRET_KEY, async function(err, decodedToken) {
     if(err) {
      throw err;
     }
     else {
      const user = await getUser(Number(decodedToken._id));
      if (!user.admin) {
        throw new Error("User is not an administrator");
      }
     }
   });
   (req as CustomRequest).token = decoded;

   next();
 } catch (err) {
   res.status(401).send(err.message);
 }
};
